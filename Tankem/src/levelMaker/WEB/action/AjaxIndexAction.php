<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	require_once("action/CommonAction.php");
	require_once("action/DAO/DTO.php");
	require_once("action/DAO/DAO.php");

	class AjaxIndexAction extends CommonAction {

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction(){
			
			if(!empty($_POST['joueurs'])){
				$listeJoueur = $_POST['joueurs']; //[0] = x, [1] = y  
				$listeBlocs = $_POST['listeBlocs']; //Chaque bloque [] est un array qui contient [0] = X, [1] = Y, [2] = IDbloque
				$nomCarte = $_POST['nomCarte'];
				$maxX = $_POST['maxX']; //MAX COLONNEs
				$maxY = $_POST['maxY']; //MAX LIGNES
				$status = $_POST['status'];
				$min_temps_apparition_item  = $_POST['minVit'];//$_POST['min_temps_apparition_itemstatus'];
				$max_temps_apparition_item  =  $_POST['maxVit'];//$_POST['max_temps_apparition_itemstatus'];
			//TEST
    		//Remplacer par les valeurs POST (Exempl en haut)

    		//$min_temps_apparition_item : de 0 a
    		//$max_temps_apparition_item : de 1 a 60
    		//($listJoueur, $listBloque, $nomCarte, $maxX, $maxY, $status, $min_temps_apparition_item, $max_temps_apparition_item)  // 
   			$dto = new DTO($listeJoueur, $listeBlocs, $nomCarte, $maxX, $maxY, $status, $min_temps_apparition_item, $max_temps_apparition_item);
    		$dao = new DAO();
    		$dao->getData($dto);
    		$dao->writeInBD();
			}
			

	}
		/*
		function verificationData(){
        if(strlen($nomCarte) >= 20)
            return false;
        if($this->DTO->maxX < 6 || $this->DTO->maxX > 12 || $this->DTO->maxY < 6 || $this->DTO->maxY > 12)
            return false;
        if($this->DTO->min_temps_apparition_item < 0 || $this->DTO->max_temps_apparition_item)
            return false;
        foreach ($this->DTO->listJoueur as $joueur) {
            if($joueur[0] < 0 || $joueur[0] >= $this->DTO->maxX || $joueur[1] < 0 || $joueur[1] >= $this->DTO->maxX)
                return false;
        }
        foreach ($this->DTO->listBloque as $bloque) {
            if($bloque[0] < 0 || $bloque[0] >= $this->DTO->maxX || $bloque[1] < 0 || $bloque[1] >= $this->DTO->maxX)
                return false;
            //Il manque regarder si le bloc est possible (0 a 8)
        }
        //Regarder aussi si min_temps_apparition_item est pas négatif ou supérieur a $max_temps_apparition_item
           
        return true;
    }*/

}
