<?php
	require_once("action/DAO/Connection.php");

	class UserDAO {

		public static function authenticate($username, $pwd) {
			$connection = Connection::getConnection();
			$statement = $connection->prepare('SELECT * FROM '.TABLE_USER.' where USERNAME = ?');
			$statement->bindParam(1, $username);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$visibility = 0;

			if ($row = $statement->fetch()) {
				if (password_verify($pwd, $row["password"])) {
					$_SESSION["firstname"] = $row["firstname"];
					if($row["role"] == "member"){
						$visibility = 1;
					}

					elseif($row["role"] == "admin")
						$visibility = 2;
				}
			}
			return $visibility;
		}

		public static function register($email, $username, $firstname, $lastname, $password, $country){
			$pass = password_hash($password, PASSWORD_DEFAULT);
			$connection = Connection::getConnection();
			try{
				$sql = 'INSERT INTO '.TABLE_USER.' (email, username, firstname, lastname, password, country) VALUES(?, ?, ?, ?, ?, ?)';
				$statement = $connection->prepare($sql);
				$statement->bindParam(1, $email);
				$statement->bindParam(2, $username);
				$statement->bindParam(3, $firstname);
				$statement->bindParam(4, $lastname);
				$statement->bindParam(5, $pass);
				$statement->bindParam(6, $country);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
			}
			catch(Exception $e){
				exit('<b>Exception Ligne : '. $e->getLine() .' (code : '. $e->getCode() .') :</b> '. $e->getMessage());
			}

		}

		public static function insertReponse($reponse, $author){
				$connection = Connection::getConnection();
				try{
					$statement = $connection->prepare('INSERT INTO stack_answers(name, reponse) VALUES(?, ?)');
					$statement->bindParam(1, $author);
					$statement->bindParam(2, $reponse);
					$statement->setFetchMode(PDO::FETCH_ASSOC);
					$statement->execute();
				}
				catch(Exception $e){
					exit('<b>Exception Ligne : '. $e->getLine() .' (code : '. $e->getCode() .') :</b> '. $e->getMessage());
				}
			}

			public static function supprimer($id){
				$connection = Connection::getConnection();
				try{
					$statement = $connection->prepare('DELETE FROM stack_answers WHERE id = ?');
					$statement->bindParam(1, $id);
					$statement->execute();
				}
				catch(Exception $e){
					exit('<b>Exception Ligne : '. $e->getLine() .' (code : '. $e->getCode() .') :</b> '. $e->getMessage());
				}
			}



	}
