<?php

class DTO{
    function __construct($listJoueur, $listBloque, $nomCarte, $maxX, $maxY, $status, $min_temps_apparition_item, $max_temps_apparition_item){
        $this->listJoueur = $listJoueur;
        $this->listBloque = $listBloque;
        $this->nomCarte = $nomCarte;
        $this->maxX = $maxX;
        $this->maxY = $maxY;
        $this->status = $status;
        $this->min_temps_apparition_item = $min_temps_apparition_item;
        $this->max_temps_apparition_item = $max_temps_apparition_item;
    }
}