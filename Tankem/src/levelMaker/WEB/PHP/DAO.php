<?php
require_once("Connection.php");

class DAO{
    private $connection;
    private $DTO;
    private $id;

    function __construct(){
        $this->connection = Connection::getConnection();
    }

    function getData($DTO){
        $this->DTO = $DTO;
    }

    function writeInBD(){
        try {
            $this->writeCarte();
            $this->writeJoueur();
            $this->writeBloque();
        }
        catch(Exception $e) {
            echo 'Exception -> ';
            var_dump($e->getMessage());
        }
    }

    function writeCarte(){
        $statement = $this->connection->prepare("INSERT INTO carte(nom, maxX, maxY, status, min_temps_apparition_item, max_temps_apparition_item) "
            ."VALUES(?, ?, ?, ?, ?, ?) RETURNING id INTO ?");
        $statement->bindParam(1, $this->DTO->nomCarte);
        $statement->bindParam(2, $this->DTO->maxX);
        $statement->bindParam(3, $this->DTO->maxY);
        $statement->bindParam(4, $this->DTO->status);
        $statement->bindParam(5, $this->DTO->min_temps_apparition_item);
        $statement->bindParam(6, $this->DTO->max_temps_apparition_item);
        $statement->bindParam(7, $this->id, PDO::PARAM_INT, 8);

        $statement->execute();
        $this->id = (int)$this->id;
    }

    function writeJoueur(){
        foreach ($this->DTO->listJoueur as $joueur) {
            $statement = $this->connection->prepare("INSERT INTO position_joueur(id_carte, x, y) "
            ."VALUES(?, ?, ?)");
            $statement->bindParam(1, $this->id);
            $statement->bindParam(2, $joueur[0]);
            $statement->bindParam(3, $joueur[1]);

            $statement->execute();
        }
    }

    function writeBloque(){
        foreach ($this->DTO->listBloque as $bloque) {
            $statement = $this->connection->prepare("INSERT INTO bloque_carte(id_carte, x, y, id_bloque_type) "
            ."VALUES(?, ?, ?, ?)");
            $statement->bindParam(1, $this->id);
            $statement->bindParam(2, $bloque[0]);
            $statement->bindParam(3, $bloque[1]);
            $statement->bindParam(4, $bloque[2]);

            print $this->id;
            print $bloque[2];

            $statement->execute();
        }
    }
    
}
