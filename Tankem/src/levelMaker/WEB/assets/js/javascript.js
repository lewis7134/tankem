﻿let selectedX = 0; //la case selectionne
let selectedY = 0;
let nbLignes = 0;
let nbColonnes = 0;
let nameChanged = false;
let mapSet = false;

window.onload = function(){

  let lignes = 0;
  let colonnes = 0;
  let currentTuile = "tuileVide";
	let currentJoueur = "";
	let nbJoueurs = 0;
	let nbJoueursMax = 2;
	let arrayJoueurs = [];
	
	/****** LEGENDE ******/
	$("#openLegende").click(function(){
	 $("#legende").slideDown(1000);
	});
	
	$("#closeLegende").click(function(){
		$("#legende").slideUp(1000);
	});
	
	keyboard();  // binding clavier
	/**** EDITER LE NOM DE LA CARTE *****/
  $("#levelName").click(function(){
    $(this).attr("contenteditable", true);
    $(this).keypress(function (e) {
 			var key = e.which;
			if(key == 13){ // ENTER KEY
					e.preventDefault;
          if($(this).text().length <= 20)
            $(this).blur();
          else{
            $(this).text("Nom de la carte");
            $("#warning").text("Nom de carte trop long");
            $("#warning").slideDown(1000).delay(3000).slideUp(1000);
          } 
			}
		});
  }).blur(function(){
    if($(this).text().length <= 20){
      $(this).attr("contenteditable", false);
      nameChanged = true;
    }
    else{
      $(this).text("Nom de la carte");
      $("#warning").text("Nom de carte trop long");
      $("#warning").slideDown(1000).delay(3000).slideUp(1000);
    }
  });
  
	//****** VALIDE LE NOMBRE DE BLOCS ET CRÉER LA MAP ******
  $("#createMap").click(function(){
    lignes = $("#lignes").val();
    colonnes = $("#colonnes").val();
    
    if(lignes.length === 0 || colonnes.length === 0 ){
      $("#warning").text("Veuillez insérer des valeurs");
      $("#warning").slideDown(1000).delay(3000).slideUp(1000);
    }
    else if(isNaN(lignes) || isNaN(colonnes)){
			$("#warning").text("Input Invalide (doit seulement être des numéro)");
      $("#warning").slideDown(1000).delay(3000).slideUp(1000);
		}
		else{
      nbLignes = parseInt(lignes);
      nbColonnes = parseInt(colonnes);
      
      if(respectLimits(nbLignes,nbColonnes)){
        $("#zoneCarte").html("");
				$("#zoneCarte").css("width", colonnes*60+colonnes*20);
        for(i=lignes-1; i >=0; i--){
					for(j=0; j < colonnes; j++){
						$("#zoneCarte").append('<div id='+j+'-'+i+' class="square col-lg-1 tuileVide"></div>');
				}
          $("#zoneCarte").append('<div class="col-lg-12 spacer"></div>');
				}
				mapSet = true;
				//reset selected
				selectedX = 0;
				selectedY = 0;
				addOutlineSelected();
      }
      else{
        $("#warning").text("Les valeurs ne respectent pas les limites");
        $("#warning").slideDown(1000).delay(3000).slideUp(1000);
      }//FIN RESPECT LIMITES
    }// FIN LENGTH === 0
  })//FIN CLICK VALIDER
 
 /***** TOGGLE LE TYPE DE BLOC *****/	
 $(".nav-sidebar li.btn").click(function(){
	 currentJoueur = "";
   $(".nav-sidebar li."+currentTuile).removeClass("shadow"); //ENLEVE L'ANCIENNE CASE SÉLECTIONNÉE
	 $(".nav-sidebar li .joueur").removeClass("shadow");
   $(this).addClass("shadow");
	 currentTuile = $(this).attr("class").replace('btn ','').replace('shadow', '');// AJOUTE LA NOUVELLE CASE SÉLECTIONNÉE
 });

 /***** CLICK AJOUTER JOUEURS *****/
 $(".nav-sidebar li .joueur").click(function(){
		 $(".nav-sidebar li").removeClass("shadow"); //ENLEVE L'ANCIENNE CASE SÉLECTIONNÉE#
		 $(".nav-sidebar li .joueur").removeClass("shadow");
		 $(this).addClass("shadow");
		 currentJoueur = $(this).attr("id");
		 //currentTuile = $(this).attr("class").replace('btn ','').replace('shadow', '');// AJOUTE LA NOUVELLE CASE SÉLECTIONNÉE
 	}); 
  
 /***** AJOUTE LA COULEUR OU LE JOUEUR AU BLOC *****/
 $(document).on('click', '.square', function(){
	 if(currentJoueur === ""){
      $(this).attr('class', '');
      $(this).addClass("square col-lg-1 "+currentTuile); 
	 }
	 else{
		 		deletePlayer(currentJoueur);
				arrayJoueurs.push(currentJoueur);
				$(this).text(currentJoueur);
		 		$("#"+currentJoueur).removeClass("shadow");
	 			currentJoueur = "";
	 }
 });

 /**
	* input min / max Vitesse only allow numbers
	*/
 $(".vitesses").keydown(function (e) {
		// Allow: backspace, delete
		if ($.inArray(e.keyCode, [46, 8]) !== -1 ||
					// Allow: home, end, left, right
				(e.keyCode >= 35 && e.keyCode <= 39)) {
							// let it happen, don't do anything
							return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
 });
  
/***** VALIDER *****/ // QUAND LA MAP EST PRÊTE, ON ENVOIE
$("#validate").click(function(){
	var dictSquares = {};
	var oneSquare = {}
	$(".square").each(function() {
		dictSquares[$(this).attr('id')] = $(this).attr("class").replace('square col-lg-1 ',''); // L'ID  dictSquares[x-y] = type de tuile
	});
	let listeBlocs = convertDict(dictSquares, parseInt(lignes), parseInt(colonnes));
	let joueurs = findPlayers();
	console.log("before");
	console.log(joueurs);
	console.log(nameIsChanged());
	let minV = parseInt($("#minVitesse").val());
	let maxV = parseInt($("#maxVitesse").val());


	if(maxV > minV && maxV < 60 && minV < 60 && minV > 0 && maxV > 1){
		//valide la vitesse max & min
		if(joueurs !== false && nameIsChanged()){ // SI IL Y A BIEN DEUX JOUEURS PLACÉS
			console.log("entered");
			// ENVOYER AJAX
			$.ajax({
					url:"ajaxIndex.php",
					method:"post",
					data: {'joueurs': joueurs,
								'listeBlocs':listeBlocs, 
								'nomCarte' : $("#levelName").text(), 
								'maxX' : nbColonnes,
								'maxY' : nbLignes,
								'minVit': minV,
								'maxVit': maxV, 
								'status': $("#status").val() 
								},
					dataType:"text"
			}).done(function(data){
				console.log(data);
				$("#warning").text(data);
				$("#warning").slideDown(1000).delay(3000).slideUp(1000);
				console.log(data);
				//message de validation 
				//$("#warning").text("Carte ajoutée!");
				//$("#warning").slideDown(1000).delay(3000).slideUp(1000);

				// reset la carte
				$("#zoneCarte").html("");	
				$("#zoneCarte").css("width", colonnes*60+colonnes*20);
				$("#levelName").html("Nom de la carte");
				$("#lignes").val("");
				$("#colonnes").val("");
				$("#minVitesse").val("");
				$("#maxVitesse").val("");

				mapSet = false;
				
			});
		}
	}
	else{
		$("#warning").text("Les valeurs de vitesse ne respectent pas les limites");
    $("#warning").slideDown(1000).delay(3000).slideUp(1000);
	}
 }); 
	
}// Document.ready

/***** VÉRIFICATION DES VALEURS ENTRÉES *****/
function respectLimits(nbLignes, nbColonnes){
    let maximumCol = 12;
    let minimumCol = 6;
    let maximumLine = 12;
    let minimumLine = 6;
    return ((nbColonnes <= maximumCol && nbColonnes >= minimumCol) && (nbLignes <= maximumLine && nbLignes >= minimumLine))?true:false;
  }

/***** CONVERTI LE TYPE DE BLOCS EN TABLEAU DE NOMBRE *****/
function convertDict(dictGrilleJeu, nbLigne, nbColonne){
	const dictTypeTuile = {};
		dictTypeTuile['tuileVide'] = 1;
		dictTypeTuile['tuileVideArbre'] = 2;
		dictTypeTuile['tuileMur'] = 3;
		dictTypeTuile['tuileMurArbre'] = 4;
		dictTypeTuile['murMobile'] = 5;
		dictTypeTuile['murMobileArbre'] = 6;
		dictTypeTuile['murMobileInverse'] = 7;
		dictTypeTuile['murMobileInverseArbre'] = 8;

  let bloques = []; // contient tous les bloques (returned)

    for(let y = 0;y<nbLigne;y++){
			for(let x = 0; x<nbColonne;x++){
      let key = x+'-'+y;
      let bloque = [];
      let typeString = dictGrilleJeu[key]; // typeString = type de la tuile en String ex: 'Tuile mur avec arbre'

      if(dictTypeTuile[typeString] != 1){ // si la tuile n'est pas une tuile vide
        let type = dictTypeTuile[typeString];
        bloque.push(x); // coordonné x
        bloque.push(y); // coordonné y
        bloque.push(type) // type de la tuile en numero
        bloques.push(bloque);
      }
    }
  }
  return bloques;
}

function findPlayers(){
	let players = [];
	 $(".square").each(function() {
			if($(this).text() == 1 || $(this).text() == 2 ){
				let posPlayer = [];
				let id = $(this).attr('id');
				let x = id.substr(0, id.indexOf('-'));
				let y = id.substr(id.indexOf("-") + 1);
				posPlayer.push(x);
				posPlayer.push(y);
				players.push(posPlayer);
			}
  });

	if(players.length == 2)
		return players;
	else{
		$("#warning").text("Le nombre de joueur est insuffisant");
    $("#warning").slideDown(1000).delay(3000).slideUp(1000);
		return false;
	}
	

}

function nameIsChanged(){
	if(nameChanged && $("#levelName").text() !== "Nom de la carte")
		return true;
	else{
		$("#warning").text("Vous n'avez pas changé le nom de la carte");
    $("#warning").slideDown(1000).delay(3000).slideUp(1000);
		return false;
	}
}
/***** SUPPRIME LE JOUEUR *****/
function deletePlayer(player){
	  $(".square").each(function() {
			if($(this).text() == player ) {
				$(this).text("");
			}
  });
}


//GESTION DES INPUT CLAVIER

function keyboard(){
	
	document.addEventListener("keydown", function(event){
		
		//deplacement avec les fleches
		if(mapSet){

			if(event.keyCode == 38){ //UP
				cleanPreviousSelected();
				if(selectedY < nbLignes-1){
					selectedY+=1
				}
				addOutlineSelected();	
			}
			else if(event.keyCode == 40){ // DOWN
				cleanPreviousSelected();
				if(selectedY > 0){
					selectedY-=1
				}
				addOutlineSelected();	
			}
			else if(event.keyCode == 37){ // LEFT
				cleanPreviousSelected();
				if(selectedX > 0){
					selectedX-=1
				}
				addOutlineSelected();	
			}
			else if(event.keyCode == 39){ // RIGHT
				cleanPreviousSelected();
				if(selectedX < nbColonnes-1){
					selectedX+=1
				}
				addOutlineSelected();	
			}

			//type de bloque (1-8)

			else if(event.keyCode >= 49 && event.keyCode <= 56){
				const typeBloque = ['tuileVide','tuileVideArbre','tuileMur','tuileMurArbre','murMobile','murMobileArbre','murMobileInverse','murMobileInverseArbre'];
				let selected = event.keyCode-49;
				$("li."+typeBloque[selected]).trigger("click");
			}
			
			//changer le type de la case selectionner (Enter)

			else if(event.keyCode == 13){
				event.preventDefault();
				let id = selectedX+"-"+selectedY;
				$("#"+id).trigger("click");
			}
			

			//assignation des joueurs (9 et 0)
			
			if(event.keyCode == 48){
				let id = "2";
				$("#"+id).trigger("click");
			}
			else if(event.keyCode == 57){
				let id = "1";
				$("#"+id).trigger("click");
			}
		}	
	});
}

function cleanPreviousSelected(){
  let selected = document.getElementById(selectedX+"-"+selectedY);
	selected.style.outline = "0px";
}
function addOutlineSelected(){
 let selected = document.getElementById(selectedX+"-"+selectedY);
 selected.style.outline = "2px solid red";
	
}

