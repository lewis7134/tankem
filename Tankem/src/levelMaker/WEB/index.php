<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	require_once("action/IndexAction.php");
  $action = new IndexAction();
	$action->execute();
 ?>


<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Tankem Level Editor</title>
      <meta name="description" content="Level editor">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
      
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
      <link rel="stylesheet" media="screen" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
      <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      <link rel="stylesheet" media="screen" href="assets/css/style.css">
      <script src="assets/js/javascript.js"></script>
   </head>
   <body> 
		 
		<div id="warning" class="alert alert-danger text-center" role="alert" style="display:none;">BONJOUR</div> 
		 
    <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header col-lg-12">
              <div id="navbar" class="navbar-collapse collapse">
								<div class="ro">
                <a class="navbar-brand" href="#">TANKEM LEVEL EDITOR <i class="fa fa-beer"></i></a>
               <div id="valuesMap" class="inputLinecol col-lg-7">
                <div class="form-group col-lg-4 text-center">
                  <input id="lignes" type="text" class="form-control"  placeholder="Lignes (Min = 6, Max = 12)">
                </div>
                <div class="form-group col-lg-4 text-center">
                  <input  id="colonnes" type="text" class="form-control" placeholder="Colonnes (Min = 6, Max = 12)">
                </div>
                <button id="createMap" type="submit" class="btn btn-primary col-lg-2">Créer carte <i class="fa fa-map-o"></i></button>
              </div>
								
								<div class="col-lg-2 inputLinecol">
									<button id="openLegende" type="submit" class="btn btn-success col-lg-12">Légende <i class="fa fa-list"></i></button>
								</div>
							 
						<div id="legende" class="col-lg-6 col-lg-offset-3 text-center" style="display:none;">
            <ul class="list-group">
							<li class="list-group-item active"><strong>LÉGENDE</strong>
								<button id="closeLegende" class="fa fa-window-close-o" style="position:absolute;right:5px;color:FireBrick"></button></li>
              <li class="list-group-item">Flèches : Naviguer</li>
              <li class="list-group-item">Numéros en haut du clavier : Changer le type de case </li>
              <li class="list-group-item">Enter : Appliquer le type de case</li>
            </ul><br>
						
          </div>
              </div>
            </div>
          </div>
    </nav>

    <div class="container-fluid editor">
      <div class="row">
        <div class="col-lg-9 main">
          <div class="col-lg-4 col-lg-offset-4 text-center"><h2 id="levelName" class="page-header">Nom de la carte</h2></div>
          <div class="col-lg-12">
            <div id="zoneCarte" class="row align-items-center"></div>
          </div>
        </div>
             
        
        <div class="col-lg-3  sidebar">
          <ul class="nav nav-sidebar"><br>
            <li class="text-center"><h2>TYPES DE CASE</h2></li>
            <li class="btn tuileVide"><a href="#">(1) Tuile vide</a></li><br>
            <li class="btn tuileVideArbre"><a href="#">(2) Tuile vide avec arbre</a></li><br>
            <li class="btn tuileMur"><a href="#">(3) Tuile mur</a></li><br>
            <li class="btn tuileMurArbre" style><a href="#">(4)  Tuile mur avec arbre</a></li><br>
            <li class="btn murMobile"><a href="#">(5) Mur mobile</a></li><br>
            <li class="btn murMobileArbre"><a href="#">(6) Mur mobile avec arbre</a></li><br>
            <li class="btn murMobileInverse"><a href="#">(7) Mur mobile inverse</a></li><br>
            <li class="btn murMobileInverseArbre"><a href="#">(8) Mur mobile inverse avec arbre</a></li><br>
            <li class="row">
            <button id="1" class="btn btn-primary col-lg-6 joueur">(9) Joueur 1</button>
             <button id="2" class="btn btn-warning col-lg-6 joueur">(0)  Joueur 2</button>
             <li> <input  id="minVitesse" type="text" min="0" max="60" class="vitesses form-control" placeholder="minimum Vitesse (0-60)"></li>
             <li> <input  id="maxVitesse" type="text" min="1" max="60" class="vitesses form-control" placeholder="maximum Vitesse (1-60)"></li>
             <select id="status" name="Status">
              <option value="0">Actif</option>
              <option value="1">Test</option>
              <option value="2">Inactif</option>
            </select>
             </li>
            <hr>
          </ul>
          <button id="validate" class="btn btn-primary col-lg-12">Valider <i class="fa fa-check"></i></button>
        </div>

      </div>
    </div>     
</body>
</html>