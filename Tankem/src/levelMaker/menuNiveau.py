# coding: utf-8

import sys

from direct.showbase.ShowBase import ShowBase
from direct.gui.DirectGui import *
from direct.gui.OnscreenText import OnscreenText 
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
from menuNiveau import *
import sys

import ctypes #messages derreure  
from levelMaker import DAO 

from gestion_user_et_stats import menuLogin

class MenuNiveau(ShowBase):
    def __init__(self):
        self.DAOlevel = DAO.DAOlevel()

        # check si on as une conection a la bd
        self.connectionErrorBD = self.DAOlevel.isErrorConnect()

        self.sound = loader.loadSfx("../asset/Menu/demarrage.mp3")

        #Initialisation de l'effet de transition
        curtain = loader.loadTexture("../asset/Menu/loading.jpg")

        self.transition = Transitions(loader)
        self.transition.setFadeColor(0, 0, 0)
        self.transition.setFadeModel(curtain)

        #On dit à la caméra que le dernier modèle doit s'afficher toujours en arrière
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(20)


        # Image d'arrière plan
        self.background = OnscreenImage(parent = render2d, image = '../asset/Menu/menuPrincipal.jpg')

        self.controlTextScale = 0.10
        self.controlBorderWidth = (0.005, 0.005)

        # si on as pas de connection a la base de donne
        # skip le choix de niveau
        mapsInfo = None
        if self.connectionErrorBD != False:
            mapsInfo = self.DAOlevel.getLevel().getValeur('niveau')
        else:
            ctypes.windll.user32.MessageBoxA(0, "Il est impossible de ce connecter a la base de donne, on vas loader le niveau par default", "Imposible de connecter a la bd", 0)

        self.scrollItemButtons = self.createAllItems(mapsInfo)

        # Ou la même structure avec une génération automatique.
        # mapsInfo = [('Level {0}'.format(i), i) for i in range(0, 10)]


        verticalOffsetControlButton = 0.225
        verticalOffsetCenterControlButton = -0.02


        self.myScrolledListLabel = DirectScrolledList(
                decButton_pos = (0.0, 0.0, verticalOffsetControlButton + verticalOffsetCenterControlButton),
                decButton_text = "Monter",
                decButton_text_scale = 0.08,
                decButton_borderWidth = (0.0025, 0.0025),
                decButton_frameSize = (-0.35, 0.35, -0.0375, 0.075),
                decButton_text_fg = (0.15, 0.15, 0.75, 1.0),

                incButton_pos = (0.0, 0.0, -0.625 - verticalOffsetControlButton + verticalOffsetCenterControlButton),
                incButton_text = "Descendre",
                incButton_text_scale = 0.08,
                incButton_borderWidth = (0.0025, 0.0025),
                incButton_frameSize = (-0.35, 0.35, -0.0375, 0.075),
                incButton_text_fg = (0.15, 0.15, 0.75, 1.0),

                pos = (0, 0, 0.5),

                items = self.scrollItemButtons,
                numItemsVisible = 5,
                forceHeight = 0.175,
                
                frameSize = (-1.05, 1.05, -0.95, 0.325),
                frameColor = (0.5, 0.5, 0.5, 0.75),

                itemFrame_pos = (0.0, 0.0, 0.0),
                itemFrame_frameSize = (-1.025, 1.025, -0.775, 0.15),
                itemFrame_frameColor = (0.35, 0.35, 0.35, 0.75),
                itemFrame_relief = 1
            )

        self.quitButton = DirectButton(
                text = ("Quitter", "Quitter", "Quitter", "disabled"),
                text_scale = self.controlTextScale,
                borderWidth = self.controlBorderWidth,
                relief = 2,
                pos = (0.0, 0.0, -0.75),
                frameSize = (-0.5, 0.5, -0.0625, 0.105),
                command = lambda : sys.exit(),
            )

    def createItemButton(self, mapName, mapId, maxX, maxY, minTemp, maxTemp):
        return DirectButton(
                text = mapName,
                text_scale = self.controlTextScale, 
                borderWidth = self.controlBorderWidth, 
                relief = 2,
                frameSize = (-1.0, 1.0, -0.0625, 0.105),
                command = lambda: self.loadGame(mapName, mapId, maxX, maxY, minTemp, maxTemp))

    def createAllItems(self, mapsInfo):
        aleatoire = False

        scrollItemButtons = []
        # scrollItemButtons = [self.createItemButton(u'-> Carte aléatoire <-', None, 10, 10, None, None)]

        # if 'Aleatoire' in mapsInfo[0]:
            
        if mapsInfo != None:
            for mapInfo in mapsInfo:
                # check si on avait une map aleatoire dans la BD sinon on l'ajoute
                if mapInfo[0] == 'Aleatoire':
                    scrollItemButtons = [self.createItemButton(u'-> Carte aléatoire <-', None, 10, 10, None, None)] + scrollItemButtons
                    aleatoire = True
                else:
                    scrollItemButtons.append(self.createItemButton(self.formatText(mapInfo[0]), mapInfo[1], mapInfo[2], mapInfo[3], mapInfo[4], mapInfo[5]))

        if aleatoire == False:
            scrollItemButtons = [self.createItemButton(u'-> Carte aléatoire <-', None, 10, 10, None, None)] + scrollItemButtons

        return scrollItemButtons

    def formatText(self, text, maxLength = 20):
        return text if len(text) <= maxLength else text[0:maxLength] + '...'

    def cacher(self):
        #Est esssentiellement un code de "loading"
        
        
        #On remet la caméra comme avant
        base.cam.node().getDisplayRegion(0).setSort(self.baseSort)

        #On cache les menus
        self.background.hide()
        self.quitButton.hide()
        self.myScrolledListLabel.hide()
    
    def loadGame(self, mapName, mapId, maxX, maxY, minTemp, maxTemp):
        #aller au menu du login
        self.cacher()
        self.menuLogin = menuLogin.MenuLogin(mapName, mapId, maxX, maxY, minTemp, maxTemp)