 --Mes tables sont en 3 FN, car elles sont atomique, aucune valeur n'est composé de deux
--valeurs qui pourrait être séparé. Toute les colones dépendent de la clé primaire (ici le ID)
--et finalement il y a une table de relation avec le type bloque, car il ne dépendent pas
--de la clé de bloque_carte, mais de de son ID.

DROP TABLE bloque_carte;
DROP TABLE type_bloque;
DROP TABLE position_Joueur;
DROP TABLE carte;

CREATE TABLE carte(
 id                         NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
 nom                        VARCHAR(20) NOT NULL ,
 maxX                       NUMBER NOT NULL ,
 maxY                       NUMBER NOT NULL ,
 status                     NUMBER NOT NULL ,
 min_temps_apparition_item  NUMBER NOT NULL ,
 max_temps_apparition_item  NUMBER NOT NULL ,
 date_cree                  date default sysdate not null,

 CONSTRAINT PK_carte PRIMARY KEY (id),
 CONSTRAINT c_u_carte_nom UNIQUE (nom)
);

CREATE TABLE position_joueur(
 id             NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
 id_carte       NUMBER NOT NULL ,
 x              NUMBER NOT NULL ,
 y              NUMBER NOT NULL ,

 CONSTRAINT PK_position_joueur PRIMARY KEY (id)
);

CREATE TABLE type_bloque(
 id             NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
 nom            VARCHAR(255) NOT NULL ,

 CONSTRAINT PK_type_bloque PRIMARY KEY (id)
);

CREATE TABLE bloque_carte(
 id             NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
 id_carte       NUMBER NOT NULL ,
 x              NUMBER NOT NULL ,
 y              NUMBER NOT NULL ,
 id_bloque_type NUMBER NOT NULL ,

 CONSTRAINT PK_bloque_carte PRIMARY KEY (id),
 CONSTRAINT FK_bloque_type FOREIGN KEY (id_bloque_type) REFERENCES type_bloque(id) ON DELETE CASCADE,
 CONSTRAINT FK_bloque_carteId FOREIGN KEY (id_carte) REFERENCES carte(id) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER before_insert_bloque_carte
BEFORE INSERT ON bloque_carte FOR EACH ROW
DECLARE
   maxX NUMBER;
   maxY NUMBER;
   nbBloque NUMBER;
BEGIN
    SELECT maxX INTO maxX FROM carte WHERE :NEW.id_carte = id;
    SELECT maxY INTO maxY FROM carte WHERE :NEW.id_carte = id;
    SELECT COUNT(id) INTO nbBloque FROM type_bloque;
    IF (:NEW.x < 0) OR (:NEW.x >= maxX) OR (:NEW.y < 0) OR (:NEW.y > maxY) THEN
        RAISE_APPLICATION_ERROR( -20001, 'Bloque trop haut ou trop bas.' );
    END IF;
    IF (:NEW.id_bloque_type > nbBloque) THEN
        RAISE_APPLICATION_ERROR( -20001, 'Type de Bloque incorect' );
    END IF;
END;
/

CREATE OR REPLACE TRIGGER before_insert_position_joueur
BEFORE INSERT ON position_joueur FOR EACH ROW
DECLARE
   maxX NUMBER;
   maxY NUMBER;
BEGIN
    SELECT maxX INTO maxX FROM carte WHERE :NEW.id_carte = id;
    SELECT maxY INTO maxY FROM carte WHERE :NEW.id_carte = id;
    IF (:NEW.x < 0) THEN
        :NEW.x  := 0;
    END IF;
    IF (:NEW.x >= maxX) THEN
        :NEW.x  := maxX;
    END IF;
    IF (:NEW.y < 0) THEN
        :NEW.y  := 0;
    END IF;
    IF (:NEW.y > maxY) THEN
        :NEW.y  := maxY;
    END IF;
END;
/

CREATE OR REPLACE TRIGGER before_insert_carte
BEFORE INSERT ON carte FOR EACH ROW
DECLARE
    v_min NUMBER;
BEGIN
    IF (:NEW.maxX < 6) OR (:NEW.maxX > 12) OR (:NEW.maxY < 6) OR (:NEW.maxY > 12) THEN
        RAISE_APPLICATION_ERROR( -20001, 'Maximum Incorrecte' );
    END IF;
    IF (:NEW.max_temps_apparition_item < :NEW.min_temps_apparition_item) THEN
        v_min := :NEW.min_temps_apparition_item;
        :NEW.min_temps_apparition_item := :NEW.max_temps_apparition_item;
        :NEW.max_temps_apparition_item := v_min;
    END IF;
    IF (:NEW.max_temps_apparition_item < 1) THEN
        :NEW.max_temps_apparition_item := 1;
    END IF;
    IF (:NEW.max_temps_apparition_item > 60) THEN
        :NEW.max_temps_apparition_item := 60;
    END IF;
    IF (:NEW.min_temps_apparition_item < 0) THEN
        :NEW.min_temps_apparition_item := 0;
    END IF;
    IF (:NEW.min_temps_apparition_item > 60) THEN
        :NEW.min_temps_apparition_item := 60;
    END IF;

END;
/

--Peuplement--
INSERT INTO type_bloque (nom) VALUES ('Tuile vide');
INSERT INTO type_bloque (nom) VALUES ('Tuile vide avec arbre');
INSERT INTO type_bloque (nom) VALUES ('Tuile mur');
INSERT INTO type_bloque (nom) VALUES ('Tuile mur avec arbre');
INSERT INTO type_bloque (nom) VALUES ('Mur mobile');
INSERT INTO type_bloque (nom) VALUES ('Mur mobile avec arbre');
INSERT INTO type_bloque (nom) VALUES ('Mur mobile inverse');
INSERT INTO type_bloque (nom) VALUES ('Mur mobile inverse avec arbre');

--Carte aléatoire--

INSERT INTO carte(nom, maxX, maxY, status, min_temps_apparition_item, max_temps_apparition_item)
VALUES('Aleatoire', 12, 12, 0, 3, 6);

--Carte 1--
INSERT INTO carte(nom, maxX, maxY, status, min_temps_apparition_item, max_temps_apparition_item)
VALUES('BattleMii', 6, 6, 0, 1, 3);

INSERT INTO position_joueur(id_carte, x, y) VALUES(2, 0, 4);
INSERT INTO position_joueur(id_carte, x, y) VALUES(2, 5, 1);

INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 1, 1, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 1, 2, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 1, 3, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 1, 4, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 1, 5, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 0, 5, 2);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 2, 1, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 2, 3, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 2, 4, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 2, 5, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 3, 1, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 3, 2, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 3, 3, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 3, 4, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 4, 0, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 4, 1, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 4, 2, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 4, 3, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 4, 4, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (2, 5, 0, 2);

--Carte 2--
INSERT INTO carte(nom, maxX, maxY, status, min_temps_apparition_item, max_temps_apparition_item)
VALUES('Piramide', 12, 12, 0, 3, 6);

INSERT INTO position_joueur(id_carte, x, y) VALUES(3, 0, 5);
INSERT INTO position_joueur(id_carte, x, y) VALUES(3, 11, 5);

INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 1, 1, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 1, 2, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 1, 3, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 1, 4, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 1, 5, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 1, 6, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 1, 7, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 1, 8, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 1, 9, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 1, 10, 7);

INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 2, 1, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 2, 2, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 2, 3, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 2, 4, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 2, 5, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 2, 6, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 2, 7, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 2, 8, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 2, 9, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 2, 10, 7);

INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 3, 1, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 3, 2, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 3, 3, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 3, 4, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 3, 5, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 3, 6, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 3, 7, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 3, 8, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 3, 9, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 3, 10, 7);

INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 4, 1, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 4, 2, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 4, 3, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 4, 4, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 4, 5, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 4, 6, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 4, 7, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 4, 8, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 4, 9, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 4, 10, 7);

INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 5, 1, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 5, 2, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 5, 3, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 5, 4, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 5, 5, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 5, 6, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 5, 7, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 5, 8, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 5, 9, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 5, 10, 7);

INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 6, 1, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 6, 2, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 6, 3, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 6, 4, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 6, 5, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 6, 6, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 6, 7, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 6, 8, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 6, 9, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 6, 10, 7);

INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 7, 1, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 7, 2, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 7, 3, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 7, 4, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 7, 5, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 7, 6, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 7, 7, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 7, 8, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 7, 9, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 7, 10, 7);

INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 8, 1, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 8, 2, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 8, 3, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 8, 4, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 8, 5, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 8, 6, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 8, 7, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 8, 8, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 8, 9, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 8, 10, 7);

INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 9, 1, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 9, 2, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 9, 3, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 9, 4, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 9, 5, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 9, 6, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 9, 7, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 9, 8, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 9, 9, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 9, 10, 7);

INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 10, 1, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 10, 2, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 10, 3, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 10, 4, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 10, 5, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 10, 6, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 10, 7, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 10, 8, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 10, 9, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (3, 10, 10, 7);

--Carte 3--
INSERT INTO carte(nom, maxX, maxY, status, min_temps_apparition_item, max_temps_apparition_item)
VALUES('Areana', 8, 8, 0, 6, 10);

INSERT INTO position_joueur(id_carte, x, y) VALUES(4, 3, 4);
INSERT INTO position_joueur(id_carte, x, y) VALUES(4, 4, 3);

INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 0, 0, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 0, 1, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 0, 2, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 0, 3, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 0, 4, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 0, 5, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 0, 6, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 0, 7, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 1, 0, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 1, 1, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 1, 2, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 1, 3, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 1, 4, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 1, 5, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 1, 6, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 1, 7, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 2, 0, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 2, 1, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 2, 2, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 2, 3, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 2, 4, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 2, 5, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 2, 6, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 2, 7, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 3, 0, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 3, 1, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 3, 2, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 3, 5, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 3, 6, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 3, 7, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 4, 0, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 4, 1, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 4, 2, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 4, 5, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 4, 6, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 4, 7, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 5, 0, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 5, 1, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 5, 2, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 5, 3, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 5, 4, 7);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 5, 5, 5);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 5, 6, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 5, 7, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 6, 0, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 6, 1, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 6, 2, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 6, 3, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 6, 4, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 6, 5, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 6, 6, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 6, 7, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 7, 0, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 7, 1, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 7, 2, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 7, 3, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 7, 4, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 7, 5, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 7, 6, 3);
INSERT INTO bloque_carte (id_carte, x, y, id_bloque_type) VALUES (4, 7, 7, 3);


COMMIT;
