#coding: utf-8
import cx_Oracle
from singleton_connection import *
from DTOlevel import *

class DAOlevel(object):

    #constructor    
    def __init__(self):
        self.conn = Connect()
        self.connexion = self.conn.connectionOracle()
        

    def isErrorConnect(self):
        return self.connexion

    def getLevel(self):
        try:
            if self.connexion != False :
                cur = self.connexion.cursor()
            else : 
                return False

            dto = DTOlevel()
            cur.execute('SELECT nom, id, maxX, maxY, min_temps_apparition_item, max_temps_apparition_item FROM carte WHERE status = 0 ORDER BY nom')
            messages = cur.fetchall()

            dto.setValeur('niveau', messages)
            return dto

        except cx_Oracle.DatabaseError as exception:
            print('Erreur readValues BD \n')
            print (exception)
            return False
        self.conn.disconnect()
    
    def getJoueurs(self, id):
        try:
            if self.connexion != False :
                cur = self.connexion.cursor()
            else : 
                return False

            dto = DTOlevel()
            sql = "SELECT x, y FROM position_joueur WHERE id_carte = :id"
            cur.execute(sql, {'id':id})
            messages = cur.fetchall()
            
            dto.setValeur('chars', messages)
            return dto

        except cx_Oracle.DatabaseError as exception:
            print('Erreur readValues BD \n')
            print (exception)
            return False
        self.conn.disconnect()

    def getBlocks(self, id):
        try:
            if self.connexion != False :
                cur = self.connexion.cursor()
            else : 
                return False

            dto = DTOlevel()
            sql = "SELECT x, y, ID_BLOQUE_TYPE FROM bloque_carte WHERE id_carte = :id"
            cur.execute(sql, {'id':id})
            messages = cur.fetchall()
            
            dto.setValeur('map', messages)
            return dto
            
        except cx_Oracle.DatabaseError as exception:
            print('Erreur readValues BD \n')
            print (exception)
            return False
        self.conn.disconnect()