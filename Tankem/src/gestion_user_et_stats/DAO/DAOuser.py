# coding: utf-8#coding: utf-8
import cx_Oracle
from singleton_connection import *
from DTOUser import *

class DAOuser(object):

    #constructor    
    def __init__(self):
        self.conn = Connect()
        self.connexion = self.conn.connectionOracle()
        

    def isErrorConnect(self):
        return self.connexion

    def authenticate(self, uname, pwd):
        try:
            if self.connexion != False :
                cur = self.connexion.cursor()
            else : 
                return False

            dto = DTOUser()
            sql = 'SELECT count(id) FROM utilisateur WHERE nom = :nom AND mdp = :mdp'
            cur.execute(sql, {'nom':uname, 'mdp':pwd})
            messages = cur.fetchone()

            if messages[0] == 1:
                return True
            else:
                return False

        except cx_Oracle.DatabaseError as exception:
            print('Erreur readValues BD \n')
            print (exception)
            return False
        self.conn.disconnect()

    def getUser(self, uname):
        try:
            if self.connexion != False :
                cur = self.connexion.cursor()
            else : 
                return False

            dto = DTOUser()
            sql = 'SELECT id, nom, mdp, exp, motto, RAWTOHEX(couleur), vie, force, agilite, dexterite FROM utilisateur WHERE nom = :nom'
            # nomJoueur, couleur, vie, force, agilite, dexterite, xp, motto
            cur.execute(sql, {'nom':uname})
            messages = cur.fetchone()

            dto.setValeur('info', messages)
            return dto

        except cx_Oracle.DatabaseError as exception:
            print('Erreur readValues BD \n')
            print (exception)
            return False
        self.conn.disconnect()
        
    def nouvellePartie(self, Joueur1, Joueur2, Game):
        # fin de partie
        try:
            if self.connexion != False :
                cur = self.connexion.cursor()
            else :
                return False

            # dateD = "TO_DATE('" + Game.dateDebut.strftime('%Y/%m/%d %H:%M:%S') + "', 'yyyy/mm/dd hh24:mi:ss')"
            # dateF = "TO_DATE('" + Game.dateFin.strftime('%Y/%m/%d %H:%M:%S') + "', 'yyyy/mm/dd hh24:mi:ss')"
            # dateD = "TO_DATE('" + str("2018-05-16 18:14:33") + "', 'yyyy/mm/dd hh24:mi:ss')"
            # dateF = "TO_DATE('" + str("2018-05-16 18:14:49") + "', 'yyyy/mm/dd hh24:mi:ss')"
            dateD = str(Game.dateDebut.strftime('%y-%m-%d %H:%M:%S'))
            dateF = str(Game.dateFin.strftime('%y-%m-%d %H:%M:%S'))


            cur.callproc("insert_Partie",[Joueur1.id, Joueur2.id, Joueur1.xpGagne, Joueur2.xpGagne, Game.gagnant, Game.idCarte,
                                            dateD, dateF, Joueur1.distanceParcourue, Joueur2.distanceParcourue,
                                            
                                            Joueur1.arme1.tire, Joueur1.arme1.reussi, Joueur1.arme1.recu,
                                            Joueur1.arme2.tire, Joueur1.arme2.reussi, Joueur1.arme2.recu,
                                            Joueur1.arme3.tire, Joueur1.arme3.reussi, Joueur1.arme3.recu,
                                            Joueur1.arme4.tire, Joueur1.arme4.reussi, Joueur1.arme4.recu,
                                            Joueur1.arme5.tire, Joueur1.arme5.reussi, Joueur1.arme5.recu,
                                            Joueur1.arme6.tire, Joueur1.arme6.reussi, Joueur1.arme6.recu,

                                            Joueur2.arme1.tire, Joueur2.arme1.reussi, Joueur2.arme1.recu,
                                            Joueur2.arme2.tire, Joueur2.arme2.reussi, Joueur2.arme2.recu,
                                            Joueur2.arme3.tire, Joueur2.arme3.reussi, Joueur2.arme3.recu,
                                            Joueur2.arme4.tire, Joueur2.arme4.reussi, Joueur2.arme4.recu,
                                            Joueur2.arme5.tire, Joueur2.arme5.reussi, Joueur2.arme5.recu,
                                            Joueur2.arme6.tire, Joueur2.arme6.reussi, Joueur2.arme6.recu])

            # hello = str(Joueur1.arme1.tire) + " , " + str(Joueur1.arme1.reussi) + " , " + str(Joueur1.arme1.recu) + " , " + str(Joueur1.arme2.tire) + " , " + str(Joueur1.arme2.reussi) + " , " + str(Joueur1.arme2.recu) + " , " + str(Joueur1.arme3.tire) + " , " + str(Joueur1.arme3.reussi) + " , " + str(Joueur1.arme3.recu) + " , " + str(Joueur1.arme4.tire) + " , " + str(Joueur1.arme4.reussi) + " , " + str(Joueur1.arme4.recu) + " , " + str(Joueur1.arme5.tire) + " , " + str(Joueur1.arme5.reussi) + " , " + str(Joueur1.arme5.recu) + " , " + str(Joueur1.arme6.tire) + " , " + str(Joueur1.arme6.reussi) + " , " + str(Joueur1.arme6.recu)
            # hello2 = str(Joueur2.arme1.tire) + " , " + str(Joueur2.arme1.reussi) + " , " + str(Joueur2.arme1.recu) + " , " + str(Joueur2.arme2.tire) + " , " + str(Joueur2.arme2.reussi) + " , " + str(Joueur2.arme2.recu) + " , " + str(Joueur2.arme3.tire) + " , " + str(Joueur2.arme3.reussi) + " , " + str(Joueur2.arme3.recu) + " , " + str(Joueur2.arme4.tire) + " , " + str(Joueur2.arme4.reussi) + " , " + str(Joueur2.arme4.recu) + " , " + str(Joueur2.arme5.tire) + " , " + str(Joueur2.arme5.reussi) + " , " + str(Joueur2.arme5.recu) + " , " + str(Joueur2.arme6.tire) + " , " + str(Joueur2.arme6.reussi) + " , " + str(Joueur2.arme6.recu)

            # print(hello)
            # print(hello2)

            self.connexion.commit()
            # self.conn.disconnect()
            
        except cx_Oracle.DatabaseError as e:
            print("Une erreur est survenue !")
            print(e)