# coding: utf-8

import sys

from direct.showbase.ShowBase import ShowBase
from direct.gui.DirectGui import *
from direct.gui.OnscreenText import OnscreenText 
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
import sys
import threading

import ctypes #messages derreure
from gestion_user_et_stats import DAO 
import datetime

from Joueur import *
from Game import *

class MenuLogin(ShowBase):
    def __init__(self, mapName, mapId, maxX, maxY, minTemp, maxTemp):
        self.DAOuser = DAO.DAOuser()

        self.connectionErrorBD = self.DAOuser.isErrorConnect()

        self.mapName = mapName
        self.mapId = mapId
        self.maxX = maxX
        self.maxY = maxY
        self.minTemp = minTemp
        self.maxTemp = maxTemp

        self.Game = Game(self.mapId,datetime.datetime.now())

        


        self.sound = loader.loadSfx("../asset/Menu/demarrage.mp3")

        #Initialisation de l'effet de transition
        curtain = loader.loadTexture("../asset/Menu/loading.jpg")

        self.transition = Transitions(loader)
        self.transition.setFadeColor(0, 0, 0)
        self.transition.setFadeModel(curtain)

        #On dit à la caméra que le dernier modèle doit s'afficher toujours en arrière
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(20)




        # Image d'arrière plan
        self.background = OnscreenImage(parent = render2d, image = '../asset/Menu/menuPrincipal.jpg')

        self.controlTextScale = 0.10
        self.controlBorderWidth = (0.005, 0.005)

        # fields logins
        self.j1t = OnscreenText(
                text = "Joueur 1",
                scale = 0.05,
                pos = (-0.95, 0.80)
            )
        self.j1 = DirectEntry(
                width = 15,
                scale=.05,
                numLines = 1,
                pos = (-0.75, 1, 0.80),
                focus = 1,
                command = self.setFocus1
            )

        self.j1pwdt = OnscreenText(
                text = "Mot de passe",
                scale = 0.05,
                pos = (-0.95, 0.70)
            )
        self.j1pwd = DirectEntry(
                width = 15,
                scale=.05,
                numLines = 1,
                pos = (-0.75, 1, 0.70),
                command = self.checkLoginInfo1
            )


        self.j2t = OnscreenText(
                text = "Joueur 2",
                scale = 0.05,
                pos = (0.55, 0.80)
            )
        self.j2 = DirectEntry(
                width = 15,
                scale=.05,
                numLines = 1,
                pos = (0.75, 1, 0.80),
                command = self.setFocus1
            )

        self.j2pwdt = OnscreenText(
                text = "Mot de passe",
                scale = 0.05,
                pos = (0.55, 0.70)
            )
        self.j2pwd = DirectEntry(
                width = 15,
                scale=.05,
                numLines = 1,
                pos = (0.75, 1, 0.70),
                command = self.checkLoginInfo2
            )

        # fields informations
        self.message = OnscreenText(
                text = "BIENVENUE",
                scale = 0.08,
                frame = (0,0,0,5),
                pos = (0.0, 0.55),
                bg = (0.35, 0.35, 0.35, 0.75),
                
                mayChange = True
        )
        

        self.quitButton = DirectButton(
                text = ("Quitter", "Quitter", "Quitter", "disabled"),
                text_scale = self.controlTextScale,
                borderWidth = self.controlBorderWidth,
                relief = 2,
                pos = (0.0, 0.0, -0.92),
                frameSize = (-0.5, 0.5, -0.0625, 0.105),
                command = lambda : sys.exit(),
            )

        if self.connectionErrorBD  != False:
            # set user default
            self.user1In = False
            self.user2In = False
        else:
            # set user default
            self.user1In = True
            self.user2In = True
            self.ajouteField()
            self.message.setText("Impossible de ce connecter a la BD")
            


    # ne fonctionne pls :(
    def setFocus1(self, chose):
        self.j1pwd.setFocus()
    def setFocus2(self, chose):
        self.j2pwd.setFocus()

    def checkLoginInfo1(self, txt):
        if self.j1.get() != self.j2.get():
            if self.DAOuser.authenticate(self.j1.get(), txt):
                self.message.setText("Joueur 1 a été authentifier")
                self.user1In = True
                
                u1 = self.DAOuser.getUser(self.j1.get()).getValeur('info')
                print(u1[5])
                self.joueur1 = Joueur(u1[0], u1[1], u1[5], u1[6], u1[7], u1[8], u1[9], u1[3], u1[4])


                self.ajouteField()
            else:
                self.message.setText("Erreure de connection")
        else:
            self.message.setText("On ne peut pas ce connecter avec deux meme joueurs")


    def checkLoginInfo2(self, txt):
        if self.j1.get() != self.j2.get():
            if self.DAOuser.authenticate(self.j2.get(), txt):
                self.message.setText("Joueur 2 a été authentifier")
                self.user2In = True

                u2 = self.DAOuser.getUser(self.j2.get()).getValeur('info')
                print(u2)
                self.joueur2 = Joueur(u2[0], u2[1], u2[5], u2[6], u2[7], u2[8], u2[9], u2[3], u2[4])
                
                
                self.ajouteField()
                #self.j1.disable()
            else:
                self.message.setText("Erreure de connection")
        else:
            self.message.setText("On ne peut pas ce connecter avec deux meme joueurs")


    

    def changeScale():
        self.JoinGame.scale = 2

    def ajouteField(self):
        if self.user1In and self.user2In:
            
            if self.connectionErrorBD  != False:
                # on créé des joueur a partir de la BD
                pass
            else:
                # on créé des joueur par defaut
                self.joueur1 = Joueur(0, "Joueur1", "000000", 0, 0, 0, 0, 0, "Joueur1 est le meilleur")
                self.joueur2 = Joueur(0, "Joueur2", "ffffff", 0, 0, 0, 0, 0, "Joueur2 est le meilleur")


            self.vs = OnscreenText(
                text = self.joueur1.nomCalcule + " (niveau: " + str(self.joueur1.niveau) + ")" + "\nVS\n" + self.joueur2.nomCalcule + " (niveau: " + str(self.joueur2.niveau) + ")",
                scale = 0.08,
                frame = (0,0,0,5),
                pos = (0.0, 0.40),
                bg = (0.35, 0.35, 0.35, 0.75),
                mayChange = True
            )
            self.combatre = OnscreenText(
                    text = "Combattrons dans\n " + self.mapName,
                    scale = 0.08,
                    frame = (0,0,0,5),
                    pos = (0.0, 0.10),
                    bg = (0.35, 0.35, 0.35, 0.75),
                    mayChange = True
            )

            self.favoris = OnscreenText(
                text = self.joueur1.nomCalcule + "est favorisé",
                scale = 0.08,
                frame = (0,0,0,5),
                pos = (0.0, -0.10),
                bg = (0.35, 0.35, 0.35, 0.75),
                mayChange = True
            )
            
            if self.joueur1.niveau == self.joueur2.niveau:
                self.favoris.setText("")
            elif self.joueur1.niveau > self.joueur2.niveau:
                self.favoris.setText(self.joueur1.nomCalcule + " est favorisé")
            elif self.joueur2.niveau > self.joueur1.niveau:
                self.favoris.setText(self.joueur2.nomCalcule + " est favorisé")

            
            self.JoinGame = DirectButton(
                    text = ("Join Game", "Join Game", "Join Game", "disabled"),
                    text_scale = self.controlTextScale, 
                    borderWidth = self.controlBorderWidth, 
                    relief = 2,
                    pos = (0.0, 0.0, -0.72),
                    frameSize = (-0.5, 0.5, -0.0625, 0.105),
                    command = lambda: self.loadGame(self.mapId, self.maxX, self.maxY, self.minTemp, self.maxTemp, self.joueur1, self.joueur2)
                )


    def formatText(self, text, maxLength = 20):
        return text if len(text) <= maxLength else text[0:maxLength] + '...'

    def cacher(self):
        #Est esssentiellement un code de "loading"
        
        
        #On remet la caméra comme avant
        base.cam.node().getDisplayRegion(0).setSort(self.baseSort)

        #On cache les menus
        self.background.hide()
        self.quitButton.hide()
        self.JoinGame.hide()

        # messages
        self.favoris.hide()
        self.combatre.hide()
        self.vs.hide()
        self.message.hide()

        # login field
        self.j1.hide()
        self.j1t.hide()
        self.j1pwd.hide()
        self.j1pwdt.hide()
        self.j2.hide()
        self.j2t.hide()
        self.j2pwd.hide()
        self.j2pwdt.hide()
    
    def loadGame(self, mapId, maxX, maxY, minTemp, maxTemp, joueur1, joueur2):
        #On démarre!
        Sequence(Func(lambda : self.transition.irisOut(0.2)),
                SoundInterval(self.sound),
                Func(self.cacher),
                Func(lambda : messenger.send("DemarrerPartie", [mapId, maxX, maxY, minTemp, maxTemp, joueur1, joueur2, self.Game])),
                Wait(0.2), #Bug étrange quand on met pas ça. L'effet de transition doit lagger
                Func(lambda : self.transition.irisIn(0.2))
        ).start()