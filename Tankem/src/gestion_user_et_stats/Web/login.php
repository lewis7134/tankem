<?php
	require_once("action/LoginAction.php");

	$action = new LoginAction();
	$action->execute();
	
	require_once("partial/header.php");
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
?>

<div class="col-lg-10 offset-lg-1" style="min-height:100vh">
<section class="form-elegant move">
    <div class="card col-lg-4 offset-lg-4" style="background-color: rgba(255, 255, 255, 0.22);">
       <div class="card-body mx-4">
            <!--Header-->
            <div class="text-center">
                <h3 class="dark-grey-text mb-4"><strong>TANKEM</strong></h3>
            </div>

            <!--Body-->
            <div class="md-form">
                <input type="text" id="nom" class="form-control">
                <label for="Form-email1">Nom de joueur</label>
            </div>

            <div class="md-form pb-3">
                <input type="password" id="mdp" class="form-control">
                <label for="Form-pass1">Mot de passe</label>
            </div>

            <div class="text-center mb-3 mt-4">
                <button id="connect" type="button" class="btn aqua-gradient btn-rounded z-depth-3 ">Connexion</button>
            </div>
        </div>
    </div>
</section>
</div>  
            
            
    

<?php
	require_once("partial/footer.php");
