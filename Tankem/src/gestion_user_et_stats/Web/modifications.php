<div class="card-body">
   <div class="col-lg-12">
      <h4>Cette partie vous permet d'éditer les informations du joueur</h4>
   </div>
   <h4 id="usernameEdit" class="card-title"><span><i class="fa fa-wrench"></i></span></h4>
   <!--Username-->
   <p class="mt-1 mb-1"></p>
   <!--MOTTO-->
   <hr>
   <div class="row">
      <div class="col-lg-4 offset-lg-2">
         <div class="md-form">
            <input type="text" id="mottoEdit" class="form-control" placeholder=" " value="Toi et moi et Zoboomafoo!">
            <label for="mottoEdit" class="active">Phrase de punch</label>
         </div>
      </div>

	  <div class="col-lg-4 text-center">
      <div class="md-form">
        <input type="color" id="colorPicker" value""="" value="#8251FF" class="mt-2 ml-5">
        <label for="colorPicker" class="">Couleur du char</label>
      </div>
    </div>

      <div class="col-lg-6 offset-lg-3 mt-4">
         <button id="sendModifications" class="btn peach-gradient btn-rounded waves-effect waves-light">ENVOYER</button>
      </div>
   </div>
</div>