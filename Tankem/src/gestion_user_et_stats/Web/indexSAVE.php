<?php
   require_once("action/IndexAction.php");
   
   $action = new IndexAction();
   $action->execute();
   
   require_once("partial/header.php");
   ?>
<?php if($action->logged){ ?>
<a id="logout" href="?logout=true" type="button" class="btn btn-danger">LOGOUT</a>
<?php } ?>

<div id="statsCard" class="col-lg-8 offset-lg-2 mt-4">
   <ul class="nav nav-tabs nav-justified primary-color">
      <!-- Nav tabs -->
      <li id="profil" class="nav-item menuProfil">
         <a class="nav-link active" data-toggle="tab" href="#panel1" role="tab">PROFIL</a>
      </li>
      <li id="statistiques" class="nav-item menuProfil">
         <a class="nav-link" data-toggle="tab" href="#panel2" role="tab">STATISTIQUES</a>
      </li>
      <li id="modifications" class="nav-item menuProfil">
         <a class="nav-link" data-toggle="tab" href="#panel3" role="tab">MODIFICATIONS</a>
      </li>
   </ul>
   <div class="tab-content card">
      <!--Panel 1-->
      <div class="tab-pane fade in show active text-center" id="panel1" role="tabpanel">
		 <?php require_once("profil.php")?>
		 <div id="coolpoints"></div>
		 <div id = "legende">
			<p style= "margin:0px">Clique gauche pour ajouter un coolpoint</p>
			<p style= "margin:0px">Clique droit pour enlever un coolpoint</p> 
		 </div>
      </div>
      <!--Panel 2-->
      <div class="tab-pane fade text-center" id="panel2" role="tabpanel">
         <?php require_once("stats.php"); ?>
      </div>
      <!--Panel 3-->
      <div class="tab-pane fade text-center" id="panel3" role="tabpanel">
         <?php require_once("modifications.php"); ?>
      </div>
      
   </div><!-- tab-content-card -->
</div><!-- statsCard -->

<div id="statsPartie" class="col-lg-8 offset-lg-2 mt-4">
   <div class="tab-content card">
      <h2 class="text-center">
         <i class="fa fa-arrow-left float-left" onclick="retourPartie()" style="cursor:pointer"></i>		
         Partie <span id="numPartie"></span>
      </h2><hr>
		<div class="row">

			<div class="col-lg-4 offset-lg-2">
				<div class="card border-warning text-center">
					<div class="card-header text-center">DATE DÉBUT PARTIE</div>
					<div class="card-body">
						<h5 class="card-title">
							<span id="dateStart"></span><br>
							<i id="" class="fa fa fa-calendar mt-1"></i>
						</h5>
					</div>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="card border-danger text-center">
					<div class="card-header text-center">DATE FIN PARTIE</div>
					<div class="card-body">
						<h5 class="card-title">
							<span id="dateEnd"></span><br>
							<i id="" class="fa fa-calendar mt-1"></i>
						</h5>
					</div>
				</div>
			</div>
		</div>

		<div class="row mt-4">
			<div class="col-lg-4 offset-lg-2">
				<div class="card border-success text-center">
					<div class="card-header text-center">ID CARTE</div>
					<div class="card-body">
						<h5 class="card-title">
							<span id="idCarte"></span><br>
							<i id="" class="fa fa-map-marker mt-1"></i>
						</h5>
					</div>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="card border-primary text-center">
					<div class="card-header text-center">GAGNANT</div>
					<div class="card-body">
						<h5 class="card-title">
							<span id="nomGagnant"></span><br>
							<i id="" class="fa fa-trophy mt-1"></i>
						</h5>
					</div>
				</div>
			</div>
		</div><!-- row -->
   </div>
</div>
<!-- statsPartie-->
<?php require_once("partial/footer.php"); ?>
<script>
   $(document).ready(function(){
	   getData();
	   
	   meilleureArme();
	   armesPrefere();
	   precisionCarte();
	   getRepartitionParties();
	   getDates();
   })	
</script>
