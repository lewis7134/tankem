	<div class="tab-pane fade in show active text-center" id="panel1" role="tabpanel">
			<div class="card-body">
				<h4 id="username" class="card-title"></h4><!--Username-->
				<p class="mt-1 mb-1"><i class="fa fa-quote-left"></i>
				<span id="motto" data-toggle="tooltip" data-placement="right" title="Phrase de punch"></span>
				 <i class="fa fa-quote-right"></i>
				</p><!--MOTTO--><hr>
				<div class="row">

					<div class="col-lg-3">
						<div class="card border-success">
							<div class="card-header">EXPÉRIENCE</div>
							<div class="card-body text-success">
								<h5 class="card-title">
									<span id="experience"></span> PTS<br>
									<i class="fa fa-plus"></i>
								</h5>
							</div>
						</div>
					</div>

					<div class="col-lg-3">
						<div class="card border-warning">
							<div class="card-header">NIVEAU</div>
							<div class="card-body text-warning">
								<h5 class="card-title">LVL 
									<span id="niveau"></span> <br>
									<i class="fa fa-line-chart"></i>
								</h5>
							</div>
						</div>
					</div>

					<div class="col-lg-3">
						<div class="card border-danger">
							<div class="card-header">DERNIÈRE PARTIE</div>
							<div class="card-body text-danger">
								<h5 class="card-title">
									<span id="lastPartie"></span><br>
									<i class="fa fa-calendar"></i>
								</h5>
							</div>
						</div>
					</div>

					<div class="col-lg-3">
						<div class="card border-primary">
							<div class="card-header">COULEUR CHAR</div>
							<div class="card-body">
								<h5 class="card-title">
									<span id="couleur"></span><br>
									<i id="squareCouleur" class="fa fa-square"></i>
								</h5>
							</div>
						</div>
					</div>

				</div>
				
				<div class="row mt-4 cursorSet"><!--Panel-->
					<div class="col-lg-3" id="panelVie">
						<div class="card border-success box">
							<div class="card-header">VIE</div>
							<div class="card-body text-success">
								<h5 class="card-title"><span id="vie"></span> PTS</h5>
							</div>
						</div>
					</div>

					<div class="col-lg-3" id="panelForce">
						<div class="card border-warning box">
							<div class="card-header">FORCE</div>
							<div class="card-body text-warning">
								<h5 class="card-title"><span id="force"></span> PTS</h5>
							</div>
						</div>
					</div>

					<div class="col-lg-3" id="panelAgilite">
						<div class="card border-danger box">
							<div class="card-header">AGILITÉ</div>
							<div class="card-body text-danger">
								<h5 class="card-title"><span id="agilite"></span> PTS</h5>
							</div>
						</div>
					</div>

					<div class="col-lg-3" id="panelDexterite">
						<div class="card border-primary box">
							<div class="card-header">DEXTÉRITÉ</div>
							<div class="card-body text-primary">
								<h5 class="card-title"><span id="dexterite"></span> PTS</h5>
							</div>
						</div>
					</div>
				</div>
				<hr>	
		</div>
	</div>