<?php
	require_once("action/CommonAction.php");

	class IndexAction extends CommonAction {
		public $logged = false;
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			$this->logged = CommonAction::isLoggedIn();
		}
	}	
