<?php
	if(!isset($_SESSION)) 
    	session_start(); 
	require_once("action/DAO/Connection.php");

	class UserDAO {


		public static function authenticate($data) {
			$connection = Connection::getConnection();

		try{
				$statement = $connection->prepare("SELECT * FROM UTILISATEUR where NOM = ?");
				$statement->bindParam(1, $data["nom"]);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();

				$visibility = 0;

				if ($row = $statement->fetch()) {
					if ($data["mdp"] == $row["MDP"]) {
						$_SESSION["id"] = $row["ID"];
						$visibility = "connected";
					}
					else $visibility = " Erreur de Connexion ";
				}
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
			return $visibility;
		}

		public static function getUserInfos($id) {
			$connection = Connection::getConnection();

			try{
				$statement = $connection->prepare("SELECT * FROM UTILISATEUR WHERE ID = ?");
				//$statement = $connection->prepare("SELECT UTILISATEUR.*, PARTIE.DATE_FIN_PARTIE FROM UTILISATEUR INNER JOIN PARTIE ON PARTIE.ID_UTILISATEUR_GAGNANT = UTILISATEUR.ID AND UTILISATEUR.ID = ?");
				$statement->bindParam(1, $id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				
				if ($row = $statement->fetch()) {
					$json = json_encode($row);
					return $json;
				}
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
		}

		public static function setUserInfos($motto, $couleur){
			$connection = Connection::getConnection();
			try{
				$statement = $connection->prepare("UPDATE UTILISATEUR SET MOTTO=?, COULEUR=? WHERE ID = ?");
				$statement->bindParam(1, $motto);
				$statement->bindParam(2, $couleur);
				$statement->bindParam(3, $_SESSION["id"]);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->rowCount() ? true : false; //Vérifie l'update
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				$erreur = "Erreur d'update";
				return $erreur;
		  	}
		}

		public static function getPartieRecente($id){
			$connection = Connection::getConnection();
			try{
				$statement = $connection->prepare("
				SELECT TO_CHAR(date_debut_partie, 'YYYY-MM-DD HH24:MI:SS') as DATE_DEBUT_PARTIE 
				FROM partie p , PARTIE_UTILISATEUR u 
				WHERE p.id = u.ID_PARTIE and u.ID_UTILISATEUR = ? 
				ORDER BY date_debut_partie DESC");
				$statement->bindParam(1,$id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				$row = $statement->fetch(PDO::FETCH_ASSOC);
				return json_encode($row);
				
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
		}

		public static function getNbPartieGagner($id){
			$connection = Connection::getConnection();
			try{
				$statement = $connection->prepare("select count(id) partieGagner from partie where ID_UTILISATEUR_GAGNANT = ?");
				$statement->bindParam(1,$id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				$row = $statement->fetch(PDO::FETCH_ASSOC);
				return json_encode($row);
				
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
		}

		public static function getNbPartie($id){
			$connection = Connection::getConnection();
			try{
				$statement = $connection->prepare("select count(id) nbPartie from PARTIE_UTILISATEUR where ID_UTILISATEUR = ?");
				$statement->bindParam(1,$id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				$row = $statement->fetch(PDO::FETCH_ASSOC);
				return json_encode($row);
				
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
		}

		public static function getParties($id) {
			$connection = Connection::getConnection();
			try{
				$statement = $connection->prepare("SELECT ID_PARTIE FROM PARTIE_UTILISATEUR WHERE ID_UTILISATEUR = ?");
				$statement->bindParam(1, $_SESSION["id"]);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				while ($row = $statement->fetch(PDO::FETCH_ASSOC)){
					$rows[] = $row;
				}
				return json_encode($rows);
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
		}

		public static function getDetailPartie($id) {
			$connection = Connection::getConnection();
			try{
				//$statement = $connection->prepare("SELECT * FROM PARTIE WHERE ID = ?");
				$statement = $connection->prepare("
					SELECT PARTIE.*, TO_CHAR(PARTIE.date_debut_partie, 'YYYY-MM-DD HH24:MI:SS') as DATE_DEBUT_PARTIE,
					TO_CHAR(PARTIE.date_fin_partie, 'YYYY-MM-DD HH24:MI:SS') as DATE_FIN_PARTIE, UTILISATEUR.NOM 
					FROM PARTIE INNER JOIN UTILISATEUR ON
					UTILISATEUR.ID = PARTIE.ID_UTILISATEUR_GAGNANT AND PARTIE.ID = ?
				");
				$statement->bindParam(1, $id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				if ($row = $statement->fetch()) {
					$json = json_encode($row);
					return $json;
				}
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
		}

		public static function meilleureArme($nom) {
			$connection = Connection::getConnection();
			try{
				$statement = $connection->prepare("
				SELECT pua.ID_ARME, arme.NOM, MAX(pua.NB_TIRE_REUSSI)/ MAX(pua.NB_TIRE) pourcentTirReussi
				FROM partie_utilisateur pu
				INNER JOIN partie_utilisateur_arme pua
					ON pu.ID = pua.ID_PARTIE_UTILISATEUR
				INNER JOIN arme
					ON pua.ID_ARME = arme.ID
				INNER JOIN utilisateur u
					ON u.ID = pu.id_utilisateur
				WHERE u.NOM = ?
				GROUP BY pua.ID_ARME, arme.NOM
				ORDER BY pourcentTirReussi DESC
				");
				$statement->bindParam(1, $nom);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				while ($row = $statement->fetch(PDO::FETCH_ASSOC)){
					$rows[] = $row;
				}
				return json_encode($rows);
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
		}

		public static function getRepartitionParties($nom) {
			$connection = Connection::getConnection();
			try{
				$statement = $connection->prepare("
				SELECT u.nom, count(CASE p.ID_UTILISATEUR_GAGNANT WHEN pu.ID_UTILISATEUR THEN 1 END) Victoire,
					count(CASE WHEN p.ID_UTILISATEUR_GAGNANT != pu.ID_UTILISATEUR THEN 1 END) Defaite,
					count(*) Total
				FROM Partie_Utilisateur pu
				INNER JOIN Partie p
					ON p.id = pu.ID_PARTIE
				INNER JOIN Utilisateur u
					ON u.id = pu.ID_UTILISATEUR
				WHERE u.nom = ?
				GROUP BY u.nom
				");
				$statement->bindParam(1, $nom);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				while ($row = $statement->fetch(PDO::FETCH_ASSOC)){
					$rows[] = $row;
				}
				return json_encode($rows);
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
		}

		public static function armesPrefere($nom) {
			$connection = Connection::getConnection();
			try{
				$statement = $connection->prepare("
				SELECT a.nom, sum(pua.nb_tire) Nb_Tire_Total
				FROM UTILISATEUR u
				INNER JOIN Partie_Utilisateur pu
					ON u.id = pu.ID_UTILISATEUR
				INNER JOIN Partie_Utilisateur_Arme pua
					ON pua.id_partie_utilisateur = pu.id
				INNER JOIN arme a
					ON pua.id_arme = a.id 
				WHERE u.nom = ?
				GROUP BY a.nom
				ORDER BY Nb_Tire_Total DESC
				");
				$statement->bindParam(1, $nom);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				while ($row = $statement->fetch(PDO::FETCH_ASSOC)){
					$rows[] = $row;
				}
				return json_encode($rows);
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
		}

		public static function getStat($stat,$nom){
			$connection = Connection::getConnection();

			if($stat == 'vie'){
				$update = "select vie from UTILISATEUR WHERE nom = ?";
			}else if($stat == 'force'){
				$update = "select force FROM UTILISATEUR WHERE nom = ?";
			}else if($stat == 'dexterite'){
				$update = "select dexterite FROM UTILISATEUR WHERE nom = ?";
			}else{
				$update = "select agilite FROM UTILISATEUR WHERE nom = ?";
			}
				
			try{
				$statement = $connection->prepare($update);
				$statement->bindParam(1, $nom);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();

				if ($row = $statement->fetch()) {
					$json = json_encode($row);
					return $json;
				}

			}catch(Exception $e){
				var_dump($e->getMessage());
				return false;
			}
			
		}

		public static function getDates($id){
			$connection = Connection::getConnection();
			try{
				//$statement = $connection->prepare("SELECT * FROM PARTIE WHERE ID = ?");
				$statement = $connection->prepare("
					select to_char(p.date_debut_partie, 'DD/MM/YYYY') as Datepartie , count(*) as Nbpartie 
					from partie p, partie_utilisateur pu
					where p.id = pu.id_partie and pu.id_utilisateur = ?
					group by to_char(p.date_debut_partie, 'DD/MM/YYYY')
				");
				$statement->bindParam(1, $id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				while ($row = $statement->fetch(PDO::FETCH_ASSOC)){
					$rows[] = $row;
				}
				return json_encode($rows);
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
		}

		public static function getHeures($id){
			$connection = Connection::getConnection();
			try{
				//$statement = $connection->prepare("SELECT * FROM PARTIE WHERE ID = ?");
				$statement = $connection->prepare("
					select to_char(p.date_debut_partie, 'DD/MM/YYYY HH') as Datepartie , count(*) as Nbpartie 
					from partie p, partie_utilisateur pu
					where p.id = pu.id_partie and pu.id_utilisateur = ?
					group by to_char(p.date_debut_partie, 'DD/MM/YYYY HH')
				");
				$statement->bindParam(1, $id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				while ($row = $statement->fetch(PDO::FETCH_ASSOC)){
					$rows[] = $row;
				}
				return json_encode($rows);
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
		}

		public static function precisionCarte(){
			$connection = Connection::getConnection();
			try{
				$statement = $connection->prepare("SELECT c.nom Carte_Precision, 
				SUM(CASE pua.ID_ARME WHEN 1 THEN pua.NB_TIRE_REUSSI END)/SUM(CASE pua.ID_ARME WHEN 1 THEN pua.NB_TIRE END)*100 Mitraillette,
				SUM(CASE pua.ID_ARME WHEN 2 THEN pua.NB_TIRE_REUSSI END)/SUM(CASE pua.ID_ARME WHEN 2 THEN pua.NB_TIRE END)*100 Shotgun,
				SUM(CASE pua.ID_ARME WHEN 3 THEN pua.NB_TIRE_REUSSI END)/SUM(CASE pua.ID_ARME WHEN 3 THEN pua.NB_TIRE END)*100 Grenade,
				SUM(CASE pua.ID_ARME WHEN 4 THEN pua.NB_TIRE_REUSSI END)/SUM(CASE pua.ID_ARME WHEN 4 THEN pua.NB_TIRE END)*100 Piege,
				SUM(CASE pua.ID_ARME WHEN 5 THEN pua.NB_TIRE_REUSSI END)/SUM(CASE pua.ID_ARME WHEN 5 THEN pua.NB_TIRE END)*100 Guide,
				SUM(CASE pua.ID_ARME WHEN 6 THEN pua.NB_TIRE_REUSSI END)/SUM(CASE pua.ID_ARME WHEN 6 THEN pua.NB_TIRE END)*100 Spring
			FROM CARTE c
			INNER JOIN Partie p
				ON c.id = p.ID_CARTE
			INNER JOIN Partie_Utilisateur pu
				ON p.id = pu.ID_PARTIE
			INNER JOIN Partie_Utilisateur_Arme pua
				ON pu.id = pua.ID_PARTIE_UTILISATEUR
			GROUP BY c.nom");
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				while ($row = $statement->fetch(PDO::FETCH_ASSOC)){
					$rows[] = $row;
				}
				return json_encode($rows);
			}
			catch (PDOException $e) {
				var_dump($e->getMessage());
				return false;
		  	}
		}
	
		public static function updateStats($stat,$valeur,$nom){
			$connection = Connection::getConnection();
			echo "Bonjour : ";
			if($stat == 'vie'){
				$update = "update UTILISATEUR set vie = ? where nom = ?";
			}else if($stat == 'force'){
				$update = "update UTILISATEUR set force = ? where nom = ?";
			}else if($stat == 'dexterite'){
				$update = "update UTILISATEUR set dexterite = ? where nom = ?";
			}else{
				$update = "update UTILISATEUR set agilite = ? where nom = ?";
			}
				
			
			try{
				$statement = $connection->prepare($update);
				$statement->bindParam(1, $valeur);
				$statement->bindParam(2, $nom);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();

				
			}catch(Exception $e){
				var_dump($e->getMessage());
			}			
		}
	}