<?php
	require_once("action/CommonAction.php");
  	require_once("action/DAO/UserDAO.php");

	class AjaxLoginAction extends CommonAction {
		public $result;
		
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
	        if(!empty($_POST["nom"]) && !empty($_POST["mdp"]))
	        {
				$data = [];
				$data["nom"] = $_POST["nom"];
				$data["mdp"] = $_POST["mdp"];
				$this->result = UserDAO::authenticate($data);
				if($this->result == "connected")
					$_SESSION["visibility"] = CommonAction::$VISIBILITY_MEMBER;
				else{ 
					$_SESSION["visibility"] = CommonAction::$VISIBILITY_PUBLIC;
				}

			}
	        else
	        {
	        	$this->result = "Champs de login vides";  
	        }
		}
	}
