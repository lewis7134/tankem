<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/UserDAO.php");

	class AjaxIndexAction extends CommonAction {
		public $result;
		
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}
		
		protected function executeAction() {
			if(!empty($_POST["action"])){

				/************** GET DATA  **************/
				if($_POST["action"] == "getData"){
					if(!empty($_SESSION["id"]))
						$this->result = UserDAO::getUserInfos($_SESSION['id']);
					else 
						$this->result = "Session est vide";  	
				}
				/************** SET DATA  **************/
				else if($_POST["action"] == "setData"){
					if(!empty($_POST["motto"]) && !empty($_POST["couleur"]))
						$this->result = UserDAO::setUserInfos($_POST["motto"], $_POST["couleur"]);
					else
						$this->result = "Il manque une information";
				}
				/************** GET TOUTES LES PARTIES  **************/
				else if($_POST["action"] == "getParties"){
					if(!empty($_SESSION["id"]))
						$this->result = UserDAO::getParties($_SESSION['id']);
					else
						$this->result = "Il manque une information";
				}
				/****************Partie la plus recente **************/
				else if($_POST["action"] == "getPartieRecente"){
					if(!empty($_SESSION["id"]))
						$this->result = UserDAO::getPartieRecente($_SESSION['id']);
					else
						$this->result = "Il manque une information";
				}
				/**************** NB partie gagner **************/
				else if($_POST["action"] == "getNbPartieGagner"){
					if(!empty($_SESSION["id"]))
						$this->result = UserDAO::getNbPartieGagner($_SESSION['id']);
					else
						$this->result = "Il manque une information";
				}
				/**************** Nb partie joueur **************/
				else if($_POST["action"] == "getNbPartie"){
					if(!empty($_SESSION["id"]))
						$this->result = UserDAO::getNbPartie($_SESSION['id']);
					else
						$this->result = "Il manque une information";
				}
				/************** GET UNE SEULE PARTIE  **************/
				else if($_POST["action"] == "getDetailPartie"){
					if(!empty($_POST["idPartie"]))
						$this->result = UserDAO::getDetailPartie($_POST["idPartie"]);
					else
						$this->result = "Fournir l'id de partie";
				}	
				/************** Update stats  **************/
				else if($_POST["action"] == "update"){
					echo "username : ".(!empty($_POST["username"])).", stat : ".(!empty($_POST["stat"])).", valeur: ".$_POST["valeur"];
					if(!empty($_POST["username"]) && !empty($_POST["stat"])){
						$this->result = UserDAO::updateStats($_POST["stat"],$_POST["valeur"],$_POST["username"]);
						$this->result = print_r($_POST);
					}			
				}
				/************** get stats  **************/
				else if($_POST["action"] == "getStat"){
					if(!empty($_POST["username"]) && !empty($_POST["stat"])){
						$this->result = UserDAO::getStat($_POST["stat"],$_POST["username"]);
					}			
				}
				/************** get la meilleure arme  **************/
				else if($_POST["action"] == "meilleureArme"){
					if(!empty($_POST["username"])){
						$this->result = UserDAO::meilleureArme($_POST["username"]);
					}			
				}
				/************** get les parties gagnées, perdues, et total  **************/
				else if($_POST["action"] == "getRepartitionParties"){
					if(!empty($_POST["username"])){
						$this->result = UserDAO::getRepartitionParties($_POST["username"]);
					}			
				}
				/************** get les parties gagnées, perdues, et total  **************/
				else if($_POST["action"] == "armesPrefere"){
					if(!empty($_POST["username"])){
						$this->result = UserDAO::armesPrefere($_POST["username"]);
					}
				}
				/************** get les precision des armes par carte  **************/
				else if($_POST["action"] == "precisionCarte"){
					if(!empty($_SESSION["id"])){
						$this->result = UserDAO::precisionCarte();
					}			
				}
				else if($_POST["action"] == "getDates"){
					if(!empty($_SESSION["id"])){
						$this->result = UserDAO::getDates($_SESSION["id"]);
					}			
				}
				else if($_POST["action"] == "getHeures"){
					if(!empty($_SESSION["id"])){
						$this->result = UserDAO::getHeures($_SESSION["id"]);
					}			
				}
			}			
		}
	}
	
	