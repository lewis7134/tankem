google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.load('current', {'packages':['corechart']});
google.charts.load('current', {'packages':['annotationchart']});



/************** EFFICACITÉ DES ARMES ******************/
function efficaciteArmes(data){
      
  let j = 1;
  let arrayArmes = new Array();
  arrayArmes[0] = ['Arme', 'Efficacité'];

  $.each(JSON.parse(data), function(idx, obj) {
    arrayArmes[j] = [obj.NOM, parseFloat(obj.POURCENTTIRREUSSI.replace(",", "."))*10];
    j++;
  });

  var table = google.visualization.arrayToDataTable(arrayArmes, false);
  google.charts.setOnLoadCallback(drawBackgroundColor(table));
}

function drawBackgroundColor(tableau) {

  var options = {
    hAxis: {title: 'Arme'},
    vAxis: { title: 'Efficacité'},
    height:400,
    width:600
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
  chart.draw(tableau, google.charts.Bar.convertOptions(options));
}
/**************** END ******************/




/************** ARMES PAR ORDRE DE PRÉFÉRENCE ******************/
function armePreference(data){
      
  let j = 1;
  let arrayArmes = new Array();
  arrayArmes[0] = ['Arme', 'Préférence', { role: 'style' }];
  let prefere = "";
  $.each(JSON.parse(data), function(idx, obj) {
    arrayArmes[j] = [obj.NOM, parseInt(obj.NB_TIRE_TOTAL), 'color: BA3A2F'];
    j++;
  });
  prefere = arrayArmes[1][0];

  var table = google.visualization.arrayToDataTable(arrayArmes, false);
  google.charts.setOnLoadCallback(drawArmePreference(table, prefere));
}

function drawArmePreference(tableau, prefere) {

  var options = {
    title: "Arme préférée : "+prefere,
    hAxis: {title: 'Arme'},
    vAxis: { title: 'Préférence'},
    height:400,
    width:600,
    colors: ['#BA3A2F']
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('chart_div3'));
  chart.draw(tableau, google.charts.Bar.convertOptions(options));
}
/**************** END ******************/




/************** RÉPARTITION DES PARTIES ( WIN / LOOSE / TOTAL ) ******************/
function repartitionParties(data){

  let repartition = JSON.parse(data);
  var arrayRepartition = new google.visualization.DataTable();
  arrayRepartition.addColumn('string', 'Type');
  arrayRepartition.addColumn('number', 'Pourcentage');
  arrayRepartition.addRows([
                            ['Victoire', parseInt(repartition[0].VICTOIRE)],
                            ['Défaite', parseInt(repartition[0].DEFAITE)]
                          ]);

  let totalParties = parseInt(repartition[0].VICTOIRE)+parseInt(repartition[0].DEFAITE);
  google.charts.setOnLoadCallback(drawChartRepartitions(arrayRepartition, totalParties));
}

function drawChartRepartitions(arrayRepartition, totalParties) {

  

  var options = {
    title: 'Total des parties jouées : '+totalParties,
    pieHole: 0.4,
    height:400,
    width:600
  };

  var chart = new google.visualization.PieChart(document.getElementById('chart_div2'));
  chart.draw(arrayRepartition, options);
}
/**************** END ****************/




/************** TIMELINE DATES ******************/
function datesTimeline(data){
  let j = 1;
  let dates = new Array();
  dates[0] = ['Date', 'Partie jouées par jour'];
  $.each(JSON.parse(data), function(idx, obj) {
    var date = obj.DATEPARTIE.split("/");
    var datePartie = new Date(date[2],date[1],date[0]);
    dates[j] = [datePartie, parseInt(obj.NBPARTIE)];
    j++;
  });
  var table = google.visualization.arrayToDataTable(dates, false);
  google.charts.setOnLoadCallback(drawTimelineDates(table));
}

function drawTimelineDates(table) {
  var chart = new google.visualization.AnnotationChart(document.getElementById('chart_div4'));
  var options = {
    displayAnnotations: true,
    height:350,
    width:700
  };
  chart.draw(table, options);
}
/**************** END ******************/


/************** TIMELINE HEURES ******************/
function heureTimeline(data){
  let j = 1;
  let dates = new Array();
  dates[0] = ['Date', 'Partie jouées par heure'];
  $.each(JSON.parse(data), function(idx, obj) {
    var dateTime = obj.DATEPARTIE.split(" ");
    var date = dateTime[0].split("/");
    var time = dateTime[1];
    var datePartie = new Date(date[2],date[1],date[0], time, 0, 0, 0);
    dates[j] = [datePartie, parseInt(obj.NBPARTIE)];
    j++;
  });
  var table = google.visualization.arrayToDataTable(dates, false);
  google.charts.setOnLoadCallback(drawTimelineHeures(table));
}

function drawTimelineHeures(table) {
  var chart = new google.visualization.AnnotationChart(document.getElementById('chart_div5'));
  var options = {
    displayAnnotations: true,
    height:350,
    width:700
  };
  chart.draw(table, options);
}
/**************** END ******************/