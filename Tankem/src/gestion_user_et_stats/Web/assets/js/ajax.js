let inAjax = false;

$(document).ready(function(){

	
	if (localStorage["username"] != null) {
		$('#username').val(localStorage["username"]);
	}

	$(document).keypress(function(e){
	    if (e.which == 13){
	        $("#connect").click();
	    }
	});
	
	/********** CONNEXION **********/
	$("#connect").on('click', function (e) {
		if($("#nom").val() !== "" && $("#mdp").val() !== ""){
			console.log("CONNECT");
			$.ajax({
					url:"ajaxLogin.php",
					method:"post",
					data: {'nom': $('#nom').val(),
						'mdp': $('#mdp').val() },
					dataType:"text",
			}).done(function(data){
				console.log("data : "+data)
				if(data == " Problème de connexion")
					toastr.error("Problème de connexion");
				else if(JSON.parse(data) == "connected"){
					localStorage.setItem("username",$('#nom').val());
					$("body").fadeOut("slow", function(){
						$(location).attr('href', 'index.php');
					});
				}
				else
					toastr.error("Mauvais login/password");
			}).fail(function(data){
				toastr.error("Erreur Appel Ajax");
			});
		}//END IF
	});

	/****** MISE A JOUR A CHAQUE CHANGEMENT D'ONGLET *****/
	$(".menuProfil").on('click', function (e) {
		var id = $(this).attr("id");
		if(id == "profil"){
			getData();
		}
		else if(id == "statistiques"){
			getParties();
		}
		else if(id == "modifications"){}
	});

	$("#sendModifications").on('click', function (e) {
		var motto = $("#mottoEdit").val();
		var couleur = $("#colorPicker").val();
		couleur = couleur.substr(1);

		if(motto.length == 0 || couleur.length == 0)
			toastr.error('Remplir correctement les champs');
		else{
			toastr.info('Envoi des informations');
			setData(motto, couleur);
		}
	});

	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	})

});//DOCUMENT READY


/****** REMPLIR INFORMATIONS DU USER******/
function getData(){
	$("#statsCard").fadeTo( "3000", 1 );
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	'action':'getData' },
		dataType:"text",
	}).done(function(data){
		var user = JSON.parse(data);
		getPartieRecente();
		$("#username").text(user.NOM);
		$("#usernameEdit").attr('value', user.NOM);
		$("#motto").text(user.MOTTO);
		$("#mottoEdit").attr('value', user.MOTTO);
		$("#experience").text(user.EXP);
		var niveau = Math.round(Math.sqrt(parseInt(user.EXP) - 50) / (5 * Math.sqrt(2)) - 1);
		(niveau > 0)? $("#niveau").text(niveau) : $("#niveau").text(0);
		$("#vie").text(user.VIE);
		$("#force").text(user.FORCE);
		$("#agilite").text(user.AGILITE);
		$("#dexterite").text(user.DEXTERITE);
		$("#couleur").text(user.COULEUR).css("color", "#"+user.COULEUR);
		$("#squareCouleur").css("color","#"+user.COULEUR);
		$("#colorPicker").attr("value", "#"+user.COULEUR);
		$("#coolpoints").text("Coolpoints restant: "+getCoolpoints(user.EXP,user.AGILITE,user.FORCE,user.VIE,user.DEXTERITE));

		callStatistiques();

	}).fail(function(data){
		toastr.error("Erreur appel getData()");
	});
}

/****** APPEL DES FONCTIONS POUR RECEVOIR LES STATISTIQUES *******/
function callStatistiques(){
	meilleureArme();
	armesPrefere();
	precisionCarte2();
	getRepartitionParties();
	getDates();
	getHeures();
}

/****** SAUVEGARDER MODIFICATIONS DU USER******/
function setData(motto, couleur){
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	
				'action':'setData',
				'motto': motto,
				'couleur':couleur
			  },
		dataType:"text",
	}).done(function(data){
		if(data || !data)
			toastr.success('Informations envoyées');
		else
			toastr.error('Problème update');
	}).fail(function(data){
		toastr.error("Erreur appel setData()");
	});
}

/****** SAUVEGARDER MODIFICATIONS DU USER******/
function modifierStat(stat,valeur){
	let reponse = JSON.parse(data);
	let rep = 0;
	let id = 0;
	if(stat == "vie"){rep = reponse.VIE; id = "vie"}
	else if(stat == "force"){rep =reponse.FORCE;id = "force"}
	else if(stat == "dexterite"){rep =reponse.DEXTERITE;id = "dexterite"}
	else{rep =reponse.AGILITE;id = "agilite"}

	if(modif == true){
		modifierStat(stat,parseInt(rep)+valeur);
	}else{
		document.getElementById(id).innerText = rep;
		modifCoolpointsAjax();
	}
}

function VerifCoolpointsAjax(stat,valeur){
	if(!inAjax){
		inAjax = true;
		$.ajax({
			url:"ajaxIndex.php",
			method:"post",
			data: {	'action':'getData' },
			dataType:"text",
		}).done(function(data){
			var user = JSON.parse(data);
			if(valeur == 1){
				if((getCoolpoints(user.EXP,user.AGILITE,user.FORCE,user.VIE,user.DEXTERITE)-1)>-1){
					getStat(stat,valeur,true);
				}else{
					inAjax = false;
				}
				
			}else{
				getStat(stat,valeur,true);
			}
		}).fail(function(data){
			toastr.error("Erreur appel VerifCoolpointsAjax()");
		});
	}
}

// refresh le champ txt indiquant le nombre restant de coolpoints a depenser
function modifCoolpointsAjax(){
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	'action':'getData' },
		dataType:"text",
	}).done(function(data){
		var user = JSON.parse(data);
		$("#coolpoints").text("Coolpoints restant: "+getCoolpoints(user.EXP,user.AGILITE,user.FORCE,user.VIE,user.DEXTERITE));

	}).fail(function(data){
		toastr.error("Erreur appel modifCoolpointsAjax()");
	});
}

/******MODIFICATIONS DES STATS DU JOUEUR******/
function getStat(stat,valeur,modif){ // stat = quelque statisque voulez-vous / valeur = combien voulez-vous ajouter ou enlever / modif = une fois l'operation terminer voulez-vous retourner la valeur ou lancer la fonction modif  
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {'username': localStorage["username"],
				'stat':stat,
				'action':"getStat"
		},
		dataType:"text",
	}).done(function(data){
		let reponse = JSON.parse(data);
		let rep = 0;
		let id = 0;
		if(stat == "vie"){rep = reponse.VIE; id = "vie"}
		else if(stat == "force"){rep =reponse.FORCE;id = "force"}
		else if(stat == "dexterite"){rep =reponse.DEXTERITE;id = "dexterite"}
		else{rep =reponse.AGILITE;id = "agilite"}

		if(modif == true){
			modifierStat(stat,parseInt(rep)+valeur);
		}else{
			document.getElementById(id).innerText = rep;
			modifCoolpointsAjax();
			inAjax = false;
		}
		

	}).fail(function(data){
		toastr.error("Erreur appel getStats()");
	});
}

function modifierStat(stat,valeur){

	if(valeur > -1 && valeur < 21 ){
		$.ajax({
			url:"ajaxIndex.php",
			method:"post",
			data: {'username': localStorage["username"],
					'stat':stat,
					'valeur':valeur,
					'action':"update"
			},
			dataType:"text",
		}).done(function(data){
			getStat(stat,0,false);
		}).fail(function(data){
			toastr.error("Erreur appel modifierStats()");
		});
	}else{
		inAjax = false;
	}
}

function getPartieRecente(){
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {
				'action':'getPartieRecente'
			  },
		dataType:"text",
	}).done(function(data){
		let date = JSON.parse(data);
		let champ = document.getElementById("lastPartie");
		if(date != false){
			champ.innerText = date.DATE_DEBUT_PARTIE;
		}else{
			champ.innerText = "NA";
			$("#statistiques a").prop("disabled",true);
			toastr.warning("Vous n'avez aucune partie jouer");
		}
	}).fail(function(data){
		toastr.error("Erreur appel getPartieRecente()");
	});
}

function getParties(){
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	
				'action':'getParties'
			  },
		dataType:"text",
	}).done(function(data){
		$.each(JSON.parse(data), function(idx, obj) {
			var partie ='<a href="#" class="list-group-item list-group-item-action" onclick="showDetailsPartie('+obj.ID_PARTIE+')">Partie '+obj.ID_PARTIE+'</a>';
			$("#listeParties").append(partie);
		});
		if(data || !data)
			toastr.success('Informations envoyées');
		else
			toastr.error('Problème update');
	}).fail(function(data){
		toastr.error("Erreur appel setData()");
	});
}

function getDates(){
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	
				'action':'getDates'
			  },
		dataType:"text",
	}).done(function(data){
		datesTimeline(data);
	}).fail(function(data){
		toastr.error("Erreur appel getDates()");
	});
}

function getHeures(){
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	
				'action':'getHeures'
			  },
		dataType:"text",
	}).done(function(data){
		heureTimeline(data);
	}).fail(function(data){
		toastr.error("Erreur appel getHeures()");
	});
}

function precisionCarte2(){
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	
				'action':'precisionCarte'
			  },
		dataType:"text",
	}).done(function(data){
		let precision = JSON.parse(data);
		let table = $("#stats2 .row");
		table.empty();
		
		$.each(JSON.parse(data), function(idx, obj) {
			let name = ["Mitraillette", "Grenade", "Guide", "Piege", "Shotgun", "Spring"];
			let array = [];
			array = [obj.MITRAILLETTE, obj.GRENADE, obj.GUIDE, obj.PIEGE, obj.SHOTGUN, obj.SPRING];
			let li = "";
			for(var i=0; i<array.length; i++){
				li += '<li class="list-group-item d-flex justify-content-between align-items-center">'+name[i]+
						'<span class="badge badge-pill light-blue">'+fixNan(array[i])+' %</span>'+
					  '</li>'};
			var toInsert = '<div class="col-lg-3 mb-2">'+
						   	'<div class="card">'+                  
								'<div class="card-body">'+obj.CARTE_PRECISION+
									'<ul class="list-group mt-2">'+li+'</ul>'+              
								'</div></div></div><br>';
			table.append(toInsert);
		});			
	}).fail(function(data){
		toastr.error("Erreur appel setData()");
	});
}

function fixNan(nb){
	nb = parseFloat(nb).toFixed(0);
	if(nb == "NaN"){
		nb = 0; 
		return nb;
	}
	else
		return nb;
}

function getNbPartie(){
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	
				'action':'getNbPartie'
			  },
		dataType:"text",
	}).done(function(data){
		let nb = JSON.parse(data)
		console.log("nb partie jouer: "+nb.NBPARTIE);
	}).fail(function(data){
		toastr.error("Erreur appel getNbPartie()");
	});
}

function getNbPartieGagner(){	
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	
				'action':'getNbPartieGagner'
			  },
		dataType:"text",
	}).done(function(data){
		let gagner = JSON.parse(data)
		console.log("nb partie ganger : " +gagner.PARTIEGAGNER);
	}).fail(function(data){
		toastr.error("Erreur appel getNbPartieGagner()");
	});
}

function getRepartitionParties(){
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	
				'action':'getRepartitionParties',
				'username':localStorage.getItem("username")
			  },
		dataType:"text",
	}).done(function(data){
		repartitionParties(data);
	}).fail(function(data){
		toastr.error("Erreur appel getNbPartieGagner()");
	});
}

function armesPrefere(){
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	
				'action':'armesPrefere',
				'username':localStorage.getItem("username")
			  },
		dataType:"text",
	}).done(function(data){
		armePreference(data);
	}).fail(function(data){
		toastr.error("Erreur appel getNbPartieGagner()");
	});
}

function meilleureArme(){
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	
				'action':'meilleureArme',
				'username':localStorage.getItem("username")
			  },
		dataType:"text",
	}).done(function(data){
		efficaciteArmes(data);
		if(data || !data){}
		else
			toastr.error('Meilleure Arme Fail');
	}).fail(function(data){
		toastr.error("Erreur appel meilleureArme()");
	});
}

function showDetailsPartie(id){
	$.ajax({
		url:"ajaxIndex.php",
		method:"post",
		data: {	
				'action':'getDetailPartie',
				'idPartie':id
			  },
		dataType:"text",
	}).done(function(data){
		var partie = JSON.parse(data);
		console.log(partie);
		$("#dateStart").text(partie.DATE_DEBUT_PARTIE);
		$("#dateEnd").text(partie.DATE_FIN_PARTIE);
		$("#idCarte").text(partie.ID_CARTE);
		$("#nomGagnant").text(partie.NOM);

		if(data || !data)
			toastr.success('Informations reçues');
		else
			toastr.error('Problème showDetailsPartie');
	}).fail(function(data){
		toastr.error("Erreur appel showDetailsPartie()");
	});

	/**** EFFET VISUEL ****/
	var childWidth = $('#statsCard').width();
	var pageWidth = $('body').width();
	var offSet = (pageWidth - childWidth)/2 + childWidth;
	$('#statsCard').animate({left: '-=' + offSet}, 800, function(){
		$('#statsCard').hide();
		$("#numPartie").text(id);
		$("#statsPartie").fadeTo( "3000", 1 );
	});
}

function retourPartie(){
	var childWidth = $('#statsCard').width()-20;
	var pageWidth = $('body').width();
	var offSet = (pageWidth - childWidth)/2 + childWidth;
	$("#statsPartie").fadeOut("800", function(){
		$('#statsCard').show();
		$('#statsCard').animate({left: '+=' + offSet}, 800);
	});
}

function afficherMsg(message) {
	$("#errorMessage").text(message);
	$("#errorDiv").slideDown(2000).delay(3000).slideUp(2000);
}
