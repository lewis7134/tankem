window.onload = function(){
	document.getElementById("panelVie").oncontextmenu = (e) =>{stopEvent(e); VerifCoolpointsAjax("vie",-1);}
	document.getElementById("panelForce").oncontextmenu = (e) =>{stopEvent(e);VerifCoolpointsAjax("force",-1);}
	document.getElementById("panelAgilite").oncontextmenu = (e) =>{stopEvent(e);VerifCoolpointsAjax("agilite",-1);}
	document.getElementById("panelDexterite").oncontextmenu = (e) =>{stopEvent(e);VerifCoolpointsAjax("dexterite",-1);}
	document.getElementById("panelVie").onclick = (e) =>{stopEvent(e); VerifCoolpointsAjax("vie",1);}
	document.getElementById("panelForce").onclick = (e) =>{stopEvent(e);VerifCoolpointsAjax("force",1);}
	document.getElementById("panelAgilite").onclick = (e) =>{stopEvent(e);VerifCoolpointsAjax("agilite",1);}
	document.getElementById("panelDexterite").onclick = (e) =>{stopEvent(e);VerifCoolpointsAjax("dexterite",1);}
}

function stopEvent(event){
	if(event.preventDefault != undefined)
	 event.preventDefault();
	if(event.stopPropagation != undefined)
	 event.stopPropagation();
}