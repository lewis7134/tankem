	/********** SUIVI DU CURSEUR ***********/
  var moving = true;
	var selector = $(".container-fluid");
	var xAngle = 20;
	var yAngle = 0;
	var zValue = 40; //décolle le div en profondeur
  var duration = "200ms";
  loginTransform();
  
	function loginTransform(){	
		selector.on("mousemove",function(e){
		  var XRel = e.pageX - $(this).offset().left;
			var YRel = e.pageY - $(this).offset().top;
			var width = $(this).width();
			yAngle = -(0.5 - (XRel / width)) * 30; 
			xAngle = (0.4 - (YRel / width)) * 30;
			
			if(moving) updateView($(this).children("div").children(".move"));
    });
		

		selector.on("mouseleave",function(){ /***** reset toutes les Transformations *****/
			reset(selector);	
		});

		selector.children("div").children(".move").on("click",function(){ /***** reset toutes les Transformations *****/
			moving = false;
			reset(selector);
		});
		
  }
  	
	function reset(selector){
		var div = selector.children("div").children(".move");
		div.css({"transform":"perspective(1000px) translate3d(-0%, -50%, 0px) rotateX(0deg) rotateY(0deg)","transition":"all "+duration+" linear 0s","-webkit-transition":"all "+duration+" linear 0s"});
		}
	
	function updateView(div){
		div.css({"transform":"perspective(1000px) translate3d(-0%,-50%,"+zValue+"px) rotateX(" + xAngle + "deg) rotateY(" + yAngle + "deg)","transition":"all "+duration+" linear 0s","-webkit-transition":"all "+duration+" linear 0s"});
		}	
  