

function getcoolpointsTotal(exp){
	let lvlCourant = Math.round(Math.sqrt(parseInt(exp) - 50) / (5 * Math.sqrt(2)) - 1);
	if(lvlCourant >=1){	
		let coolpointsTotal = lvlCourant*5;
		return coolpointsTotal;
	}
	else{
		return 0;
	}
}
function getCoolpoints(exp,agilite,force,vie,dexterite){
	let lvlCourant = Math.round(Math.sqrt(parseInt(exp) - 50) / (5 * Math.sqrt(2)) - 1);
	if(lvlCourant >=1){	
		let coolpointsTotal = lvlCourant*5;
		let coolPointsDepenser = parseInt(agilite) + parseInt(force) + parseInt(vie) + parseInt(dexterite);
		let coolpointsRestant = coolpointsTotal - coolPointsDepenser;
		return coolpointsRestant;
	}
	else{
		return 0;
	}
}

