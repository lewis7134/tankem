
<ul class="nav md-pills nav-justified pills-warning">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#panel11" role="tab">EFFICACITÉ<br>DES ARMES</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#panel12" role="tab">ARMES<br>PRÉFÉRÉES</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#panel13" role="tab">RÉPARTITION<br>DES PARTIES</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#panel14" role="tab">PRÉCISION<br> DES ARMES</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#panel15" role="tab">TIMELINE<br>STATISTIQUES</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#panel16" role="tab">LISTE<br>DES PARTIES</a>
    </li>
</ul>

<div class="tab-content">
    <div class="tab-pane fade in show active" id="panel11" role="tabpanel"><!--Panel 1-->
		<div id="chart_div"></div>
    </div><!--/.Panel 1-->
    
    <div class="tab-pane fade" id="panel12" role="tabpanel"><!--Panel 2-->
        <div id="chart_div3"></div>
    </div><!--/.Panel 2-->

    <div class="tab-pane fade" id="panel13" role="tabpanel"><!--Panel 3-->
        <div id="chart_div2"></div>
    </div><!--/.Panel 3-->

    <div class="tab-pane fade" id="panel14" role="tabpanel"><!--Panel 4-->
        <h4>PRÉCISION DES ARMES PAR PARTIE POUR L'ENSEMBLE DES CARTES</h4>
        <div id="stats2"><div class="row"></div></div>
    </div><!--/.Panel 4-->

    <div class="tab-pane fade" id="panel15" role="tabpanel"><!--Panel 5-->
        <div class="row">
            <div class="col-md-3">
                <ul class="nav  md-pills pills-primary flex-column" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#panel21" role="tab">JOUR</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel22" role="tab">HEURE</a>
                    </li>
                </ul>
            </div>
            <!-- Tab panels -->
            <div class="col-md-9">
                <div class="tab-content vertical">
                    <div class="tab-pane fade in show active" id="panel21" role="tabpanel"><!--Panel 1-->
                        <div id='chart_div4' style='width: 700px; height: 350px;'></div>
                    </div><!--/.Panel 1-->
                    
                    <div class="tab-pane fade" id="panel22" role="tabpanel"><!--Panel 2-->
                        <div id='chart_div5' style='width: 700px; height: 350px;'></div>
                    </div><!--/.Panel 2--> 
                </div>
            </div>
        </div><!-- Nav tabs -->
    </div><!--/.Panel 5-->
    
	<div class="tab-pane fade" id="panel16" role="tabpanel"><!--Panel 6-->
		<h3>SÉLECTIONNEZ UNE PARTIE</h3>
		<div id="listeParties" class="list-group col-lg-8 lg-offset-2">
		<!-- LES PARTIES SE REMPLISSENT ICI -->
		</div>
    </div><!--/.Panel 6-->

</div>


