      <!-- JQuery -->
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <!-- Bootstrap tooltips -->
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
      <!-- Bootstrap core JavaScript -->
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
      <!-- MDB core JavaScript -->
      <script type="text/javascript" src="assets/js/compiled.min.js"></script>  
      <script type="text/javascript" src="assets/js/upgrade.js"></script>
      <script type="text/javascript" src="assets/js/coolpoints.js"></script>
      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
      <script type="text/javascript" src="assets/js/charts.js"></script>
      <script type="text/javascript" src="assets/js/ajax.js"></script>
      <script type="text/javascript" src="assets/js/animations.js"></script>
    
    </div>
	</body>
</html>