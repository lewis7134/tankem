# coding: utf-8
import math

class Game(object):
	def __init__(self, idCarte, dateDebut):
		self.gagnant = None
		self.idCarte = idCarte
		self.dateDebut = dateDebut
		self.dateFin = None
	
	def fin(self, gagnant, dateFin):
		self.gagnant = gagnant
		self.dateFin = dateFin