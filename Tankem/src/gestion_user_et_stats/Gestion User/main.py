from ajout import * 
from suppression import * 

def optionValide(option):
	if option != 'A' and option != 'S':
		return False;
	else:
		return True; 

def redirection(option):
	if option == 'A':
		ajoutUsager()
	else: 
		suppressionUsager()


def main():
	while(True): 
		userChoice = raw_input(" Que Voulez-vous faire? (Ajouter = A / Supprimer = S / Quitter = Q) : ")
		userChoice = userChoice.upper();
		if userChoice == 'Q':
			return False
		if optionValide(userChoice):
			redirection(userChoice)
		else:
			print("Choix Invalide")
			main()




main()