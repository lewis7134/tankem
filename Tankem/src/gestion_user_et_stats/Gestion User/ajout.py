from DAO import * 
import re


def validationNom(input):
	if len(input) > 30 or len(input)<1:
		return False

def validationMdp(input):
	if len(input) > 30 or len(input)<1:
		return False

def validationMotto(input):
	if len(input) > 255 or len(input)<1:
		return False

def validationCouleur(input):
	if len(input) > 6 or len(input) < 6:
		return False
	elif valHexa(input) :
		return False

def valHexa(input):
	hexaPattern = re.compile( r'^([0-9a-fA-F]{6})$')
	if re.match(hexaPattern, input) == None:
		return True 
	
def ajoutUsager():
	dao = DAO()
	if(dao.connexion != False):

		boucleur = True
		while(boucleur):
			nom = raw_input("Nom de l'usager : ")
			nom = nom.strip()
			if validationNom(nom) == False:
				print("Invalide!")
			else:
				boucleur = False

				
		boucleur = True
		while(boucleur):		
			mdp = raw_input("mot de passe : ")
			mdp = mdp.strip()
			if validationMdp(mdp)==False:
				print("Invalide!")
			else:
				boucleur = False

		boucleur = True		
		while(boucleur):
			motto = raw_input("phrase punchd de l'usager : ")
			motto = motto.strip()
			if validationMotto(motto)==False:
				print("Invalide!")
			else:
				boucleur = False


		boucleur = True
		while(boucleur):
			couleur = raw_input("la couleur qui lui sera assigner (Hexa) : ")
			couleur = couleur.strip()
			if validationCouleur(couleur)==False:
				print("Invalide!")
			else:
				boucleur = False


		dto = DTO(nom,mdp,motto,couleur)
		dao.ajoutUsager(dto)



