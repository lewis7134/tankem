from DAO import * 


def validationNom(input):
	if len(input) > 30:
		return False


def suppressionUsager():
	dao = DAO()

	if(dao.connexion != False):

		boucleur = True
		while(boucleur):
			usager = raw_input("Quelle usager voulez-vous supprimer? ")
			if validationNom(usager) == False:
				print("Erreur!")
			else:
				boucleur = False

		dao.suppressionUsager(usager)
