#coding: utf-8
import cx_Oracle

class Connect():
    def __init__(self, username, password, hostname, port, database):
        self.username = username
        self.password = password
        self.hostname = hostname
        self.port = port
        self.database = database

    """ CONNECTION BD """ 
    def connectionOracle(self):
        dsn_tns = cx_Oracle.makedsn(self.hostname, self.port, self.database )
        chaineConnexion = self.username + '/' + self.password + '@' + dsn_tns
        try:
            connexion = cx_Oracle.connect(chaineConnexion)
            self.database = connexion
            return connexion
        except cx_Oracle.DatabaseError as exception:
            print(exception)
            print('Erreur de Connexion \n')
            return False

    """ DECONNEXION BD """
    def disconnect(self):
        try:
            self.database.close()
        except cx_Oracle.DatabaseError:
            print ("Erreur déconnexion")
            raise
            