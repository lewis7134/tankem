import Constants as co
import cx_Oracle
from ConnectDB import *
from DTO import *

class DAO():
	def __init__(self):
		self.conn = Connect(co.USER_DB, co.USER_PASS, co.HOSTNAME, co.PORT, co.DB)
		self.connexion = self.conn.connectionOracle()
		
	def ajoutUsager(self,dto):
		try:
			if self.connexion != False :
				cur = self.connexion.cursor()
			else :
				return False

				
			row = dto.getUtilisateurAjout()
			id = 0
			cur.callproc("insert_Utilisateur",[row[0],row[1],row[2],row[3],id])
			self.connexion.commit()
			self.conn.disconnect()
			print('Ajout de lutilisateur terminer')

		except:
			print("Le nom choisie est deja utilisez. Vous devez en choisir un autre")

	def suppressionUsager(self,usager):
		try:
			if self.connexion != False :
				cur = self.connexion.cursor()
			else :
				return False
			
			sql2 = "SELECT * FROM utilisateur WHERE nom = :id"
			cur.execute(sql2,{'id':usager})
			a = cur.fetchall();
			if len(a) > 0:
				sql = "DELETE FROM utilisateur WHERE nom = :id"
				cur.execute(sql, {'id':usager})
			

				self.connexion.commit()
				self.conn.disconnect()
				print('Suppression de lutilisateur terminer')
			else:
				print("L'usager n'existe pas !")
		except:
			print("Une erreur est survenue !")
	
