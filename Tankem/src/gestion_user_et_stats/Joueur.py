# coding: utf-8
import math
from Arme import *
from util import *

class Joueur(object):
    def __init__(self, id, nomJoueur, couleur, vie, force, agilite, dexterite, xp, motto):
		self.minVal = 0
		self.maxVal = 20

		self.id = id
		self.couleur = couleur
		self.vie = clamp(vie, self.minVal, self.maxVal)
		self.MaxVie = self.vie
		self.force = clamp(force, self.minVal, self.maxVal)
		self.agilite = clamp(agilite, self.minVal, self.maxVal)
		self.dexterite = clamp(dexterite, self.minVal, self.maxVal)
		self.nomCalcule = self.calculNom(nomJoueur)
		self.xp = xp
		self.xpGagne = 0
		self.motto = motto
		self.niveau = self.calculNiveau(xp)

		self.distanceParcourue = 0

		# Mitraillette
		self.arme1 = Arme()
		# Shotgun
		self.arme2 = Arme()
		# Grenade
		self.arme3 = Arme()
		# Piege
		self.arme4 = Arme()
		# Guide
		self.arme5 = Arme()
		# Spring
		self.arme6 = Arme()
	
    def ajouteDistance(self, dist):
		self.distanceParcourue += dist

    def calculNiveau(self, xp):
		# opperation terniere pour prevenir crash (racine carre de < 0)
		return int(round(math.sqrt(xp - 50) / (5 * math.sqrt(2)) - 1)) if xp >= 50 else 0
	
    def calculNom(self, nomJoueur):
		nomCalc = ""
		maxPoints = 20

		if (self.vie == maxPoints and self.force == maxPoints and self.agilite == maxPoints and self.dexterite == maxPoints):
			nomCalc = "Dominateur"
		else:
			nomA = ""
			nomB = ""

			dicti = {'vie': self.vie, 'force': self.force, 'agilite': self.agilite, 'dexterite': self.dexterite}
			order = sorted(dicti.items(), key=lambda x:x[1], reverse=True)

			if (order[0][0] == "vie"):
				nomA = self.calcVie(order[0][1], 1)
			elif (order[0][0] == "force"):
				nomA = self.calcForce(order[0][1], 1)
			elif (order[0][0] == "agilite"):
				nomA = self.calcAgilite(order[0][1], 1)
			elif (order[0][0] == "dexterite"):
				nomA = self.calcDexterite(order[0][1], 1)


			if (order[1][0] == "vie"):
				nomB = self.calcVie(order[1][1], 2)
			elif (order[1][0] == "force"):
				nomB = self.calcForce(order[1][1], 2)
			elif (order[1][0] == "agilite"):
				nomB = self.calcAgilite(order[1][1], 2)
			elif (order[1][0] == "dexterite"):
				nomB = self.calcDexterite(order[1][1], 2)

			nomCalc = nomA + " " + nomB

		return nomJoueur + " " + nomCalc
		
    def calcVie(self, pts, tour):
		nom = ""

		if pts >= 10:
			if tour == 1:
				nom = "l'immortel"
			else:
				nom = "immortel"
		elif pts >= 5:
			if tour == 1:
				nom = "le pétulant"
			else:
				nom = "pétulant"
		elif pts >= 1:
			if tour == 1:
				nom = "le fougeux"
			else:
				nom = "fougueux"
		
		return nom

    def calcForce(self, pts, tour):
		nom = ""

		if pts >= 10:
			if tour == 1:
				nom = "le tout puissant"
			else:
				nom = "Marc-André"
		elif pts >= 5:
			if tour == 1:
				nom = "le hulk"
			else:
				nom = "brutal"
		elif pts >= 1:
			if tour == 1:
				nom = "le crossfiter"
			else:
				nom = "qui fait du crossfit"
		
		return nom

    def calcAgilite(self, pts, tour):
		nom = ""

		if pts >= 10:
			if tour == 1:
				nom = "le foudroyant"
			else:
				nom = "foudroyant"
		elif pts >= 5:
			if tour == 1:
				nom = "le lynx"
			else:
				nom = "lynx"
		elif pts >= 1:
			if tour == 1:
				nom = "le prompt"
			else:
				nom = "prompt"
		
		return nom

    def calcDexterite(self, pts, tour):
		nom = ""

		if pts >= 10:
			if tour == 1:
				nom = "le chirurgien"
			else:
				nom = "chirurgien"
		elif pts >= 5:
			if tour == 1:
				nom = "l'habile"
			else:
				nom = "habile"
		elif pts >= 1:
			if tour == 1:
				nom = "le précis"
			else:
				nom = "précis"
		
		return nom

    def moinsVie(self, vieMoin):
		self.vie -= vieMoin
		if self.vie < 0:
			self.vie = 0

    def fin(self, xpPlus):
		self.xpGagne = xpPlus