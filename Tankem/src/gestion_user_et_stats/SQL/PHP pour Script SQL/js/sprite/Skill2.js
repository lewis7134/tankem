class Skill2 {
    constructor(x, y) {

        this.x = x;
		this.y = y;
		this.row = 0;
		
		this.sprite = new TiledImage("images/explo.png", 8, 3, 25, true, 2);
        this.sprite.changeRow(0);
        this.sprite.changeMinMaxInterval(0, 8);
    }

    tick() {
		this.sprite.tick(ctx, this.x, this.y);
		console.log(this.row + " : " + this.sprite.imageCurrentCol);
		if(this.sprite.imageCurrentCol >= 7){
			this.sprite.changeRow(++this.row);
			this.sprite.changeCol(0);
			this.sprite.changeMinMaxInterval(0, 8);
		}
			
		if(this.row == 3)
			return false;
        return true;
	}
}