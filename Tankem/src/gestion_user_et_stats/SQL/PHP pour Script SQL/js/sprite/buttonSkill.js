class ButtonSkill {
    constructor(nom, imagename) {
        this.x = 0;
        this.y = 0;
        this.height = 50;
        this.width = 50;
        this.nom = nom;

        this.image = new Image();
		this.image.src = "images/"+imagename;

    }

    tick(x, y) {
        this.x = x;
        this.y = y;
        ctx.drawImage(this.image, this.x, this.y, this.height, this.width);

        return true;
    }
}