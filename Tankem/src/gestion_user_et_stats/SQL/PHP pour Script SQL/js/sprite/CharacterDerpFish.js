class CharacterDerpFish {
    constructor(row) {
		this.scale = 1;
		this.enSkill = false;

        this.sprite = new TiledImage("images/derpfish.png", 10, 9, 50, true, this.scale);
        this.sprite.changeRow(row);
		this.sprite.changeMinMaxInterval(0, 10);

        this.x = canvasWidth*0.12;
		this.y = canvasHeight*0.8;

    }

    tick() {
		this.x = canvasWidth*0.12;
        this.y = canvasHeight*0.8;
		this.sprite.tick(ctx, this.x, this.y);
		
		if(this.enSkill && this.sprite.imageCurrentCol == 0){
			this.sprite = new TiledImage("images/derpfish.png", 10, 9, 50, true, this.scale);
        	this.sprite.changeRow(1);
			this.sprite.changeMinMaxInterval(0, 10);
			this.enSkill = false;
		}

        return true;
	}
	
	normal(){
		this.sprite = new TiledImage("images/derpfish.png", 10, 9, 150, true, this.scale);
        this.sprite.changeRow(4);
		this.sprite.changeMinMaxInterval(0, 6);
		this.enSkill = true;
	}
		
}