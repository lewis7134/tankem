class Fish {
	constructor() {
		let row = parseInt(Math.random() *2);
		let col = parseInt(Math.random() *4);

		this.sprite = new TiledImage("images/fish.png", 12, 8, 50, true, this.scale);
		if(row == 0)
			this.sprite.changeRow(2);
		else
			this.sprite.changeRow(6);
		this.sprite.changeMinMaxInterval(col*3, col*3+3);

		let departXouY = parseInt(Math.random() *2);

		if(departXouY){ //Y
			this.x = Math.random() * canvasWidth * 0.7;
			this.y = canvasHeight;
		}
		else{ //X
			this.x = -20;
			this.y = Math.random() * canvasHeight * 0.4 + canvasHeight * 0.6;
		}
		

		let randomSize = Math.random() * 2 + 1;
		
		this.sprite.scale = randomSize;

		this.speedY = randomSize;
		this.speedX = 3 + randomSize/2;
		
	}

	tick() {
		let alive = true;

		this.x += this.speedX;
		this.y -= this.speedY;
		
		this.sprite.tick(ctx, this.x, this.y);

		if (this.y < -1 * this.randomSize || this.x > canvasWidth + 20) {
			alive = false;
		}

		return alive;
	}
}