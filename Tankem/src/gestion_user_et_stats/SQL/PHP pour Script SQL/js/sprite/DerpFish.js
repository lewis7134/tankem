class DerpFish {
    constructor(row) {
		this.scale = 0.3;
		this.scaleSpeed = 0.0015;

        this.sprite = new TiledImage("images/derpfish.png", 10, 9, 50, true, this.scale);
        this.sprite.changeRow(row);
		this.sprite.changeMinMaxInterval(0, 10);
		
		this.versDevant = false;
		this.changerSprit = false;

        this.x = -30;
        this.y = 90;
		this.speedX = canvasWidth/1000;
		this.speedY = canvasHeight/5000;
		this.speedApprocheX = canvasWidth/5000;
		this.speedApprocheY = canvasHeight/1000;

		this.pourcentageEcranPivot = canvasWidth * 0.8;

    }

    tick() {
		
		if(this.x > this.pourcentageEcranPivot && !this.changerSprit){
			this.changerSprit = true;
			this.sprite.changeRow(0);
			this.versDevant = true;
		}

		if(this.versDevant){
			this.x -= this.speedApprocheX;
			this.y += this.speedApprocheY;
			this.scale += this.scaleSpeed;
			this.sprite.setScale(this.scale);
		}
		else if(!this.versDevant){
			this.x += this.speedX;
			this.y += this.speedY;
		}

		if(this.y > canvasHeight+200){
			return false;
		}

        this.sprite.tick(ctx, this.x, this.y);

        return true;
    }
}