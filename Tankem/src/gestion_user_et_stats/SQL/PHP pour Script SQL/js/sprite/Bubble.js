class Bubble {
	constructor() {
		this.x = Math.random() * canvasWidth;
		this.y = canvasHeight;
		this.size = Math.random() * 8 + 2;
		let randomSize = Math.random() * 30;
		this.height = randomSize + 10;
		this.width = randomSize + 10;
		this.speedY = this.size/3;
		this.speedX = Math.random() * 4 - 2;
		this.image = new Image();
		this.image.src = "images/bubble.png";
	}

	tick() {
		let alive = true;

		this.x += this.speedX;
		this.y -= this.speedY;
		if (this.image.complete) {
			ctx.drawImage(this.image, this.x, this.y, this.height, this.width);
		}

		if (this.y < -1 * this.height) {
			alive = false;
		}

		return alive;
	}
}