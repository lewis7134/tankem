class AttaqueBulle {
    constructor(x, y, ciblex, cibley) {

        this.x = x;
		this.y = y;
		this.ciblex = ciblex;
		this.cibley = cibley;
		
		this.image = new Image();
		this.image.src = "images/bubble.png";

		this.vitesseEnX = (this.ciblex - this.x) / (this.y - this.cibley);

    }

    tick() {
		this.x += this.vitesseEnX*3;
		this.y -= 3;
		ctx.drawImage(this.image, this.x, this.y, 50, 50);
		if(this.x > this.ciblex || this.y < this.cibley)
			return false;

        return true;
	}
}