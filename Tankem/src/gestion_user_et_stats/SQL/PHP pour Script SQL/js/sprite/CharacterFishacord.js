class CharacterFishacord {
    constructor() {
        this.scale = 2;
        this.new = false;
        this.setStartingPoint = true;

        this.sprite = new TiledImage("images/fishacord.png", 12, 1, 50, true, this.scale);
        this.sprite.changeRow(0);
        this.sprite.changeMinMaxInterval(0, 12);

        this.vitesseEnX = 0;
    }

    tick() {
        if(this.new){
            if(this.setStartingPoint){
                this.x = -50;
                this.y = canvasHeight+50;
                this.vitesseEnX = (canvasWidth*0.7 - this.x) / (this.y - canvasHeight*0.75);
                this.setStartingPoint = false;
            }
            this.x += this.vitesseEnX*2;
            this.y -= 2;
            if(this.x > canvasWidth*0.7 || this.y < canvasHeight*0.75)
                this.new = false;
        }
        else{
            this.setStartingPoint = true;
            this.x = canvasWidth*0.7;
            this.y = canvasHeight*0.75;
        }

        this.sprite.tick(ctx, this.x, this.y);

        return true;
    }
}