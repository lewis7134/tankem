class Boss {
    constructor() {
        this.scale = 2;
        this.skipe = false;
        this.currentCol = 0;
        this.maxFrameIdle = 4;
        this.enSkill = false;

        this.sprite = new TiledImage("images/boss.png", this.maxFrameIdle, 1, 120, true, this.scale);
        this.sprite.changeRow(0);
        this.sprite.changeMinMaxInterval(0, this.maxFrameIdle);

        this.x = canvasWidth*0.8;
        this.y = canvasHeight*0.3;

    }

    tick() {
        if(this.enSkill)
            this.x = canvasWidth*0.8-30;
        else
            this.x = canvasWidth*0.8;
        this.y = canvasHeight*0.3;
        this.sprite.tick(ctx, this.x, this.y);
        
        if(this.enSkill && this.sprite.imageCurrentCol == 0){
			this.sprite = new TiledImage("images/boss.png", this.maxFrameIdle, 1, 120, true, this.scale);
            this.sprite.changeRow(0);
            this.sprite.changeMinMaxInterval(0, this.maxFrameIdle);
			this.enSkill = false;
		}

        return true;
    }

    attaque(){
        this.sprite = new TiledImage("images/bossAttaque.png", 5, 1, 120, true, this.scale);
        this.sprite.changeRow(0);
        this.sprite.changeMinMaxInterval(0, 5);
        this.enSkill = true;
    }
}