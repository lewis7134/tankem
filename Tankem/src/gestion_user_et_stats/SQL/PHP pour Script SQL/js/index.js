let positionBackground = 0;
let nbUser = 3;

window.onload = () => {
	document.body.style.backgroundImage = "url(./images/lobby.jpg)";
	document.body.style.backgroundSize = 3000+"px";
	document.body.style.backgroundPosition = positionBackground+"px";
	fillTableFirst();
	mainTick();
}

function mainTick() {
	document.body.style.backgroundPosition = --positionBackground+"px";

	if(document.getElementById("nbUser").value != nbUser){
		fillTable();
	}

	window.requestAnimationFrame(mainTick);
}

function fillTable(){
	nbUser = document.getElementById("nbUser").value;

	let names = document.getElementById("names");
	let mdps = document.getElementById("mdps");
	let mottos = document.getElementById("mottos");
	let colors = document.getElementById("colors");

	let txt = "";
	let txt2 = "";
	let txt3 = "";
	let txt4 = "";
	for (let index = 0; index < nbUser; index++) {
		txt += "<div class=\"form-label\"><label for=\"names"+index+"\">Nom "+index+": </label></div>"
		txt += "<div class=\"form-input\" id=\"names\""+index+"><input type=\"input\" id=\"names" + index + "\" name=\"names" + index + "\"  /></div>";

		txt2 += "<div class=\"form-label\"><label for=\"mdps"+index+"\">MDP "+index+": </label></div>"
		txt2 += "<div class=\"form-input\" id=\"mdps\""+index+"><input type=\"input\" id=\"mdps" + index + "\" name=\"mdps" + index + "\" /></div>";

		txt3 += "<div class=\"form-label\"><label for=\"mottos"+index+"\">Motto "+index+": </label></div>"
		txt3 += "<div class=\"form-input\" id=\"mottos\""+index+"><input type=\"input\" id=\"mottos" + index + "\" name=\"mottos" + index + "\" /></div>";

		txt4 += "<div class=\"form-label\"><label for=\"colors"+index+"\">Couleur "+index+": </label></div>"
		txt4 += "<div class=\"form-input\" id=\"colors\""+index+"><input type=\"color\" id=\"colors"+index+"\" name=\"colors" + index + "\" value=\"#8251FF\" class=\"setMeto23\"></div>";
		
	}
	
	
	names.innerHTML = txt;
	mdps.innerHTML = txt2;
	mottos.innerHTML = txt3;
	colors.innerHTML = txt4;
}

function fillTableFirst(){

	let names = document.getElementById("names");
	let mdps = document.getElementById("mdps");
	let mottos = document.getElementById("mottos");
	let colors = document.getElementById("colors");

	let tabNoms = ["Jean", "Zoboomafoo", "EricPlus"];
    let tabMdps = ["aaa", "bbb", "ccc"];
    let tabMottos = ["Jean qui?", "Toi et moi et Zoboomafoo!", "EriC++"];
    let tabColors = ["#f651ff", "#8251ff", "#49ff5c"];

	
	let txt = "";
	let txt2 = "";
	let txt3 = "";
	let txt4 = "";
	for (let index = 0; index < nbUser; index++) {
		txt += "<div class=\"form-label\"><label for=\"names"+index+"\">Nom "+index+": </label></div>"
		txt += "<div class=\"form-input\" id=\"names\""+index+"><input type=\"input\" id=\"names" + index + "\" name=\"names" + index + "\" value=\""+tabNoms[index]+"\"/></div>";

		txt2 += "<div class=\"form-label\"><label for=\"mdps"+index+"\">MDP "+index+": </label></div>"
		txt2 += "<div class=\"form-input\" id=\"mdps\""+index+"><input type=\"input\" id=\"mdps" + index + "\" name=\"mdps" + index + "\" value=\""+tabMdps[index]+"\"/></div>";

		txt3 += "<div class=\"form-label\"><label for=\"mottos"+index+"\">Motto "+index+": </label></div>"
		txt3 += "<div class=\"form-input\" id=\"mottos\""+index+"><input type=\"input\" id=\"mottos" + index + "\" name=\"mottos" + index + "\" value=\""+tabMottos[index]+"\"/></div>";

		txt4 += "<div class=\"form-label\"><label for=\"colors"+index+"\">Couleur "+index+": </label></div>"
		txt4 += "<div class=\"form-input\" id=\"colors\""+index+"><input type=\"color\" id=\"colors"+index+"\" name=\"colors" + index + "\" value=\""+tabColors[index]+"\" class=\"setMeto23\"></div>";
		
	}
	
	
	names.innerHTML = txt;
	mdps.innerHTML = txt2;
	mottos.innerHTML = txt3;
	colors.innerHTML = txt4;
}