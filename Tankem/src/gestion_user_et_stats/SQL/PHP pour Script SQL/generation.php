<?php
    require_once("Connection.php");
    echo "Démarage en cour...<br>";

    $errorCaught = false;

    $noms = [];
    $mdps = [];
    $mottos = [];
    $colors = [];

    $nombreUser = $_POST["nbUser"]*1;
    $nombrePartie = $_POST["nbPartie"];
    for ($i=0; $i < $nombreUser; $i++) { 
        $noms[$i] = $_POST["names".$i];
        $mdps[$i] = $_POST["mdps".$i];
        $mottos[$i] = $_POST["mottos".$i];
        $colors[$i] = substr($_POST["colors".$i], 1, 6);
    }
    
    //$noms = array("Jean", "Zoboomafoo", "EricPlus");
    //$mdps = array("aaa", "bbb", "ccc");
    //$mottos = array("Jean qui?", "Toi et moi et Zoboomafoo!", "EriC++");
    //$colors = array("f651ff", "8251ff", "49ff5c");

    $dateDepart = $_POST["dateDepart"];//"2018-01-01 00:00:01";
    $tempDunGameMin = $_POST["tempDunGameMin"];//20; //en seconde
    $tempDunGameMax = $_POST["tempDunGameMax"];//360; //en seconde
    $dateMtn = date("Y-m-d H:i:s");

    $minDistance = $_POST["minDistance"];//200;
    $maxDistance = $_POST["maxDistance"];//10000;

    $expParCoup = 10;

    $totalArme = $_POST["totalArme"];//6;
    $minNbArme = $_POST["minNbArme"];//1; //Ne pas mettre plus de 6!
    $minTire = $_POST["minTire"];//0;
    $maxTire = $_POST["maxTire"];//200;
    $maxTireReussiPour1Arme = $_POST["maxTireReussiPour1Arme"];//5;

    $connection = Connection::getConnection();
    $idsJoueurs=[];
    $randIdUser=[];
    $randTotalTire=[];
    $idsCarte=[];
    $listDate=[];

    $myfile = fopen("sql.sql", "w") or die("Unable to open file!");

    //Generation du tableau des dates aléatoires
    $string = date("Y-m-d H:i:s",strtotime($dateDepart));
    for ($i=0; $i < $nombrePartie; $i++) { 
        $date = rand(strtotime($dateDepart),strtotime($dateMtn));
        $listDate[$i] = date("Y-m-d H:i:s",$date);
    }
    usort($listDate, "date_sort");
    

    try {
        $idsCarte = getIdAllCarte($connection);
        writeUtilisateur($myfile, $nombreUser, $noms, $mdps, $mottos, $colors);
        for ($i=0; $i < $nombrePartie; $i++) {
            $randIdUser[0] = rand(0,$nombreUser-1);
            $randIdUser[1] = $randIdUser[0];
            while ($randIdUser[0] == $randIdUser[1]) {
                $randIdUser[1] = rand(0,$nombreUser-1);
            }
            
            $randTotalTire[0] = rand($minTire, $maxTire);
            $randTotalTire[1] = rand($minTire, $maxTire);

            $tabJ1Tire = array_fill(0, $totalArme, 0);
            $tabJ1Reussi = array_fill(0, $totalArme, 0);
            $tabJ1Recu = array_fill(0, $totalArme, 0);
            $tabJ2Tire = array_fill(0, $totalArme, 0);
            $tabJ2Reussi = array_fill(0, $totalArme, 0);
            $tabJ2Recu = array_fill(0, $totalArme, 0);
            
            $randNbArme1 = rand ($minNbArme,$totalArme-1); //Pour ne pas inclure le spring
            $randNbArme2 = rand ($minNbArme,$totalArme-1); //Pour ne pas inclure le spring

            $randIDArme1 = [];
            $randIDArme2 = [];
            
            //Tableau des index des armes
            for ($b=0; $b < $randNbArme1; $b++) { 
                do{
                    $temp = rand(0, $totalArme-2); //-2 Pour ne pas inclure le spring
                    $egale = false;
                    for ($v=0; $v < count($randIDArme1); $v++) { 
                        if($temp == $randIDArme1[$v]){
                            $egale = true;
                        }
                    }
                } while ($egale);
                $randIDArme1[$b] = $temp;
            }
            for ($b=0; $b < $randNbArme2; $b++) { 
                do{
                    $temp = rand(0, $totalArme-2); //-2 Pour ne pas inclure le spring
                    $egale = false;
                    for ($v=0; $v < count($randIDArme2); $v++) { 
                        if($temp == $randIDArme2[$v]){
                            $egale = true;
                        }
                    }
                } while ($egale);
                $randIDArme2[$b] = $temp;
            }


            for ($b=0; $b < $randNbArme1; $b++) { 
                if($b >= $randNbArme1-1){
                    $tabJ1Tire[$randIDArme1[$b]] = $randTotalTire[0];
                }
                else{
                    $tabJ1Tire[$randIDArme1[$b]] = rand(0, $randTotalTire[0]);
                    $randTotalTire[0] -= $tabJ1Tire[$randIDArme1[$b]];
                }

                if($tabJ1Tire[$randIDArme1[$b]] > $maxTireReussiPour1Arme){
                    $tabJ1Reussi[$randIDArme1[$b]] = rand(0, $maxTireReussiPour1Arme);
                }
                else{
                    $tabJ1Reussi[$randIDArme1[$b]] = rand(0, $tabJ1Tire[$randIDArme1[$b]]);
                }
            }
            for ($b=0; $b < $randNbArme2; $b++) { 
                if($b >= $randNbArme2-1){
                    $tabJ2Tire[$randIDArme2[$b]] = $randTotalTire[1];
                }
                else{
                    $tabJ2Tire[$randIDArme2[$b]] = rand(0, $randTotalTire[1]);
                    $randTotalTire[1] -= $tabJ2Tire[$randIDArme2[$b]];
                }

                if($tabJ2Tire[$randIDArme2[$b]] > $maxTireReussiPour1Arme){
                    $tabJ2Reussi[$randIDArme2[$b]] = rand(0, $maxTireReussiPour1Arme);
                }
                else{
                    $tabJ2Reussi[$randIDArme2[$b]] = rand(0, $tabJ2Tire[$randIDArme2[$b]]);
                }
                
                
            }

            //Compter Coup reussi pour avoir le nombre de coup recu
            $coupRecu=[];
            $coupRecu[0]=0;
            $coupRecu[1]=0;
            for ($b=0; $b < $totalArme; $b++) { 
                $coupRecu[0] += $tabJ2Reussi[$b];
                $tabJ2Recu[$b] = $tabJ1Reussi[$b];
            }
            for ($b=0; $b < $totalArme; $b++) { 
                $coupRecu[1] += $tabJ1Reussi[$b];
                $tabJ1Recu[$b] = $tabJ2Reussi[$b];
            }

            //Compter l'exp avec les coup reussi
            $expJ1 = $coupRecu[1] * $expParCoup +50; //bonus pour le gagnat
            $expJ2 = $coupRecu[0] * $expParCoup;


            writePartieComplete($myfile, $expJ1, $expJ2, $randIdUser[0], $randIdUser[1], $idsCarte, $listDate[$i], $tempDunGameMin, $tempDunGameMax, $minDistance, $maxDistance,
                $tabJ1Tire, $tabJ1Reussi, $tabJ1Recu, $tabJ2Tire, $tabJ2Reussi, $tabJ2Recu);
        }

    }
    catch(Exception $e) {
        echo 'Exception -> ';
        var_dump($e->getMessage());
        $errorCaught = true;
    }
    if(!$errorCaught){
        echo "Insertion Réeussit!";
    }
    
    fwrite($myfile, "\nCOMMIT;");
    fclose($myfile);

    $file = "sql.sql";

   
    header('Location: '.$file);
    

    function writeUtilisateur($myfile, $nombreUser, $noms, $mdps, $mottos, $colors){
        $ids=[];
        for ($i=0; $i < $nombreUser; $i++) {
            
            $txt = "VARIABLE idJoueur".$i." NUMBER;\n";
            fwrite($myfile, $txt);
            $txt = "EXECUTE insert_Utilisateur('".$noms[$i]."', '".$mdps[$i]."', '".$mottos[$i]."', '".$colors[$i]."', :idJoueur".$i.");\n";
            fwrite($myfile, $txt);
            
        }
    }

    function writePartieComplete($myfile, $expJ1, $expJ2, $user1, $user2, $idsCarte, $date, $tempDunGameMin, $tempDunGameMax, $minDistance, $maxDistance, $tabJ1Tire, $tabJ1Reussi, $tabJ1Recu, $tabJ2Tire, $tabJ2Reussi, $tabJ2Recu){
        $randCarte = rand (0, count($idsCarte)-1);
        $randTempDuneGame = strtotime($date)+rand($tempDunGameMin, $tempDunGameMax);
        $dateFin = date("Y-m-d H:i:s",$randTempDuneGame);
        $randDistance1 = rand($minDistance, $maxDistance);
        $randDistance2 = rand($minDistance, $maxDistance);
        $txt = "EXECUTE insert_Partie(:idJoueur".$user1.", :idJoueur".$user2.", ".$expJ1.", ".$expJ2.", :idJoueur".$user1.", ".$idsCarte[$randCarte][0].", '".$date."', '".$dateFin."', ".$randDistance1.", ".$randDistance2.",".
        "   ".$tabJ1Tire[0].", ".$tabJ1Reussi[0].", ".$tabJ1Recu[0].",  ".$tabJ1Tire[1].", ".$tabJ1Reussi[1].", ".$tabJ1Recu[1].",".
        "   ".$tabJ1Tire[2].", ".$tabJ1Reussi[2].", ".$tabJ1Recu[2].",  ".$tabJ1Tire[3].", ".$tabJ1Reussi[3].", ".$tabJ1Recu[3].",".
        "   ".$tabJ1Tire[4].", ".$tabJ1Reussi[4].", ".$tabJ1Recu[4].",  ".$tabJ1Tire[5].", ".$tabJ1Reussi[5].", ".$tabJ1Recu[5].",".
        "   ".$tabJ2Tire[0].", ".$tabJ2Reussi[0].", ".$tabJ2Recu[0].",  ".$tabJ2Tire[1].", ".$tabJ2Reussi[1].", ".$tabJ2Recu[1].",".
        "   ".$tabJ2Tire[2].", ".$tabJ2Reussi[2].", ".$tabJ2Recu[2].",  ".$tabJ2Tire[3].", ".$tabJ2Reussi[3].", ".$tabJ2Recu[3].",".
        "   ".$tabJ2Tire[4].", ".$tabJ2Reussi[4].", ".$tabJ2Recu[4].",  ".$tabJ2Tire[5].", ".$tabJ2Reussi[5].", ".$tabJ2Recu[5].");\n";
        fwrite($myfile, $txt);
    }

    function getIdAllCarte($connection){
        $statement = $connection->prepare("SELECT id FROM carte");
        $statement->execute();
        return $statement->fetchall();
    }

    function date_sort($a, $b) {
        return strtotime($a) - strtotime($b);
    }