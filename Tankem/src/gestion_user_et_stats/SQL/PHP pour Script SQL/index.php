<?php
	require_once("action/indexAction.php");

	$action = new LoginAction();
	$action->execute();
	
	require_once("partial/header.php");
?>
	<script src="js/index.js" charset="utf-8"></script>
	<div class="login-container" id="scrollbar">

		<div class="login-form-frame" >
			<form action="generation.php" method="post">

                <div class="valeurs">
                    <div class="floatLeft">
                        <div class="form-label">
                            <label for="nbUser">Nombre Utilisateur:</label>
                        </div>
                        <div class="form-input">
                            <input type="number" name="nbUser" id="nbUser" step="1" value="3" min="2" max="64">
                        </div>
                    </div>

                    <div class="floatLeft">
                        <div class="form-label">
                            <label for="nbUser">Nombre de partie:</label>
                        </div>
                        <div class="form-input">
                            <input type="number" name="nbPartie" id="nbPartie" step="1" value="250" min="1" max="999999">
                        </div>
                    </div>

                    <div class="floatLeft">
                        <div class="form-label">
                            <label for="nbUser">Temps Min partie:</label>
                        </div>
                        <div class="form-input">
                            <input type="number" name="tempDunGameMin" id="tempDunGameMin" step="1" value="20" min="1" max="999998">
                        </div>
                    </div>

                    <div class="floatLeft">
                        <div class="form-label">
                            <label for="nbUser">Temps Max partie:</label>
                        </div>
                        <div class="form-input">
                            <input type="number" name="tempDunGameMax" id="tempDunGameMax" step="1" value="200" min="1" max="999999">
                        </div>
                    </div>

                    <div class="floatLeft">
                        <div class="form-label">
                            <label for="nbUser">Temps Max partie:</label>
                        </div>
                        <div class="form-input">
                            <input type="date" name="dateDepart" id="dateDepart" value="2018-01-01">
                        </div>
                    </div>

                    <div class="floatLeft">
                        <div class="form-label">
                            <label for="nbUser">Min Distance:</label>
                        </div>
                        <div class="form-input">
                            <input type="number" name="minDistance" id="minDistance" step="1" value="200" min="1" max="999999998">
                        </div>
                    </div>

                    <div class="floatLeft">
                        <div class="form-label">
                            <label for="nbUser">Max Distance:</label>
                        </div>
                        <div class="form-input">
                            <input type="number" name="maxDistance" id="maxDistance" step="1" value="10000" min="1" max="999999999">
                        </div>
                    </div>

                    <div class="floatLeft">
                        <div class="form-label">
                            <label for="nbUser">Max Tire Reussi 1 Arme:</label>
                        </div>
                        <div class="form-input">
                            <input type="number" name="maxTireReussiPour1Arme" id="maxTireReussiPour1Arme" step="1" value="5" min="1" max="100">
                        </div>
                    </div>

                    <div class="floatLeft">
                        <div class="form-label">
                            <label for="nbUser">Total Arme:</label>
                        </div>
                        <div class="form-input">
                            <input type="number" name="totalArme" id="totalArme" step="1" value="6" min="1" max="6">
                        </div>
                    </div>

                    <div class="floatLeft">
                        <div class="form-label">
                            <label for="nbUser">Min nb Arme:</label>
                        </div>
                        <div class="form-input">
                            <input type="number" name="minNbArme" id="minNbArme" step="1" value="1" min="1" max="6">
                        </div>
                    </div>

                    <div class="floatLeft">
                        <div class="form-label">
                            <label for="nbUser">Min Tire Total:</label>
                        </div>
                        <div class="form-input">
                            <input type="number" name="minTire" id="minTire" step="1" value="0" min="0" max="999998">
                        </div>
                    </div>

                    <div class="floatLeft">
                        <div class="form-label">
                            <label for="nbUser">Max Tire Total:</label>
                        </div>
                        <div class="form-input">
                            <input type="number" name="maxTire" id="maxTire" step="1" value="200" min="0" max="999999">
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <br><br>
				<div class="floatLeft" id="names"></div>
                <div class="floatLeft" id="mdps"></div>
                <div class="floatLeft" id="mottos"></div>
                <div class="floatLeft" id="colors"></div>
                <div class="clear"></div>

				<div class="form-label">
					&nbsp;
				</div>
				<div class="form-input">
					<button type="submit">Générer</button>
				</div>
			</form>
		</div>
	</div>
<?php
	require_once("partial/footer.php");
