 --Mes tables sont en 3 FN, car elles sont atomique, aucune valeur n'est composé de deux
--valeurs qui pourrait être séparé. Toute les colones dépendent de la clé primaire (ici le ID)
--et finalement il y a une table de relation avec le type bloque, car il ne dépendent pas
--de la clé de bloque_carte, mais de de son ID.

DROP TABLE partie_utilisateur_arme;
DROP TABLE partie_utilisateur;
DROP TABLE partie;
DROP TABLE arme;

CREATE TABLE partie(
 id                         NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
 date_debut_partie          date not null,
 date_fin_partie            date default sysdate not null,
 id_carte                   NUMBER NOT NULL ,
 id_utilisateur_gagnant     NUMBER NOT NULL ,
 

 CONSTRAINT PK_partie PRIMARY KEY (id),
 CONSTRAINT FK_partie_carte FOREIGN KEY (id_carte) REFERENCES carte(id) ON DELETE CASCADE,
 CONSTRAINT FK_partie_utilisateur FOREIGN KEY (id_utilisateur_gagnant) REFERENCES utilisateur(id) ON DELETE CASCADE
);

CREATE TABLE partie_utilisateur(
 id                 NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
 id_utilisateur     NUMBER NOT NULL ,
 id_partie          NUMBER NOT NULL ,
 distance_parcouru  NUMBER NOT NULL ,

 CONSTRAINT PK_p_u PRIMARY KEY (id),
 CONSTRAINT FK_p_u_partie FOREIGN KEY (id_partie) REFERENCES partie(id) ON DELETE CASCADE,
 CONSTRAINT FK_p_u_utilisateur FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id) ON DELETE CASCADE
);

CREATE TABLE arme(
 id             NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
 nom            VARCHAR(255) NOT NULL ,

 CONSTRAINT PK_arme PRIMARY KEY (id)
);

CREATE TABLE partie_utilisateur_arme(
 id                     NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
 id_partie_utilisateur  NUMBER NOT NULL ,
 id_arme                NUMBER NOT NULL ,
 nb_tire                NUMBER NOT NULL ,
 nb_tire_reussi         NUMBER NOT NULL ,
 nb_coup_recu           NUMBER NOT NULL ,

 CONSTRAINT PK_p_u_arme PRIMARY KEY (id),
 CONSTRAINT FK_p_u_arme_p_u FOREIGN KEY (id_partie_utilisateur) REFERENCES partie_utilisateur(id) ON DELETE CASCADE,
 CONSTRAINT FK_p_u_arme_arme FOREIGN KEY (id_arme) REFERENCES arme(id) ON DELETE CASCADE
);

--Peuplement--
INSERT INTO arme (nom) VALUES ('Mitraillette');
INSERT INTO arme (nom) VALUES ('Shotgun');
INSERT INTO arme (nom) VALUES ('Grenade');
INSERT INTO arme (nom) VALUES ('Piege');
INSERT INTO arme (nom) VALUES ('Guide');
INSERT INTO arme (nom) VALUES ('Spring');











CREATE OR REPLACE PROCEDURE insert_Partie (idJoueur1 NUMBER, idJoueur2 NUMBER, exp1 NUMBER, exp2 NUMBER, idJoueurGagnant NUMBER, idCarte NUMBER,
    dateDebut VARCHAR, dateFin VARCHAR, distanceJoueur1 NUMBER, distanceJoueur2 NUMBER,
    a1Tire1 NUMBER, a1TireReussi1 NUMBER, a1TireRecu1 NUMBER,
    a2Tire1 NUMBER, a2TireReussi1 NUMBER, a2TireRecu1 NUMBER,
    a3Tire1 NUMBER, a3TireReussi1 NUMBER, a3TireRecu1 NUMBER,
    a4Tire1 NUMBER, a4TireReussi1 NUMBER, a4TireRecu1 NUMBER,
    a5Tire1 NUMBER, a5TireReussi1 NUMBER, a5TireRecu1 NUMBER,
    a6Tire1 NUMBER, a6TireReussi1 NUMBER, a6TireRecu1 NUMBER,
    a1Tire2 NUMBER, a1TireReussi2 NUMBER, a1TireRecu2 NUMBER,
    a2Tire2 NUMBER, a2TireReussi2 NUMBER, a2TireRecu2 NUMBER,
    a3Tire2 NUMBER, a3TireReussi2 NUMBER, a3TireRecu2 NUMBER,
    a4Tire2 NUMBER, a4TireReussi2 NUMBER, a4TireRecu2 NUMBER,
    a5Tire2 NUMBER, a5TireReussi2 NUMBER, a5TireRecu2 NUMBER,
    a6Tire2 NUMBER, a6TireReussi2 NUMBER, a6TireRecu2 NUMBER) AS
id_partie NUMBER;
id_partie_Joueur1 NUMBER;
id_partie_Joueur2 NUMBER;
BEGIN
    IF exp1 >= 0 THEN
        UPDATE utilisateur SET exp = exp + exp1 WHERE id = idJoueur1;
    ELSE
        RAISE VALUE_ERROR;
    END IF;
    IF exp2 >= 0 THEN
        UPDATE utilisateur SET exp = exp + exp2 WHERE id = idJoueur2;
    ELSE
        RAISE VALUE_ERROR;
    END IF;
    INSERT INTO partie(date_debut_partie, date_fin_partie, id_carte, id_utilisateur_gagnant) 
        VALUES(to_date(dateDebut,'yy-mm-dd hh24:mi:ss'), to_date(dateFin,'yy-mm-dd hh24:mi:ss'), idCarte, idJoueurGagnant) RETURNING id INTO id_partie;
    INSERT INTO partie_utilisateur(id_utilisateur, id_partie, distance_parcouru)
        VALUES(idJoueur1, id_partie, distanceJoueur1) RETURNING id INTO id_partie_Joueur1;
    INSERT INTO partie_utilisateur(id_utilisateur, id_partie, distance_parcouru)
        VALUES(idJoueur2, id_partie, distanceJoueur2) RETURNING id INTO id_partie_Joueur2;
    DBMS_OUTPUT.PUT_LINE(id_partie);
    DBMS_OUTPUT.PUT_LINE(id_partie_Joueur1);
    DBMS_OUTPUT.PUT_LINE(id_partie_Joueur2);
    IF a1Tire1 > 0 OR a1TireReussi1 > 0 OR a1TireRecu1 > 0 THEN
        INSERT INTO partie_utilisateur_arme(id_partie_utilisateur, id_arme, nb_tire, nb_tire_reussi, nb_coup_recu)
            VALUES(id_partie_Joueur1, 1, a1Tire1, a1TireReussi1, a1TireRecu1);
    END IF;
    IF a2Tire1 > 0 OR a2TireReussi1 > 0 OR a2TireRecu1 > 0 THEN
        INSERT INTO partie_utilisateur_arme(id_partie_utilisateur, id_arme, nb_tire, nb_tire_reussi, nb_coup_recu)
            VALUES(id_partie_Joueur1, 2, a2Tire1, a2TireReussi1, a2TireRecu1);
    END IF;
    IF a3Tire1 > 0 OR a3TireReussi1 > 0 OR a3TireRecu1 > 0 THEN
        INSERT INTO partie_utilisateur_arme(id_partie_utilisateur, id_arme, nb_tire, nb_tire_reussi, nb_coup_recu)
            VALUES(id_partie_Joueur1, 3, a3Tire1, a3TireReussi1, a3TireRecu1);
    END IF;
    IF a4Tire1 > 0 OR a4TireReussi1 > 0 OR a4TireRecu1 > 0 THEN
        INSERT INTO partie_utilisateur_arme(id_partie_utilisateur, id_arme, nb_tire, nb_tire_reussi, nb_coup_recu)
            VALUES(id_partie_Joueur1, 4, a4Tire1, a4TireReussi1, a4TireRecu1);
    END IF;
    IF a5Tire1 > 0 OR a5TireReussi1 > 0 OR a5TireRecu1 > 0 THEN
        INSERT INTO partie_utilisateur_arme(id_partie_utilisateur, id_arme, nb_tire, nb_tire_reussi, nb_coup_recu)
            VALUES(id_partie_Joueur1, 5, a5Tire1, a5TireReussi1, a5TireRecu1);
    END IF;
    IF a6Tire1 > 0 OR a6TireReussi1 > 0 OR a6TireRecu1 > 0 THEN
        INSERT INTO partie_utilisateur_arme(id_partie_utilisateur, id_arme, nb_tire, nb_tire_reussi, nb_coup_recu)
            VALUES(id_partie_Joueur1, 6, a6Tire1, a6TireReussi1, a6TireRecu1);
    END IF;
    IF a1Tire2 > 0 OR a1TireReussi2 > 0 OR a1TireRecu2 > 0 THEN
        INSERT INTO partie_utilisateur_arme(id_partie_utilisateur, id_arme, nb_tire, nb_tire_reussi, nb_coup_recu)
            VALUES(id_partie_Joueur2, 1, a1Tire2, a1TireReussi2, a1TireRecu2);
    END IF;
    IF a2Tire2 > 0 OR a2TireReussi2 > 0 OR a2TireRecu2 > 0 THEN
        INSERT INTO partie_utilisateur_arme(id_partie_utilisateur, id_arme, nb_tire, nb_tire_reussi, nb_coup_recu)
            VALUES(id_partie_Joueur2, 2, a2Tire2, a2TireReussi2, a2TireRecu2);
    END IF;
    IF a3Tire2 > 0 OR a3TireReussi2 > 0 OR a3TireRecu2 > 0 THEN
        INSERT INTO partie_utilisateur_arme(id_partie_utilisateur, id_arme, nb_tire, nb_tire_reussi, nb_coup_recu)
            VALUES(id_partie_Joueur2, 3, a3Tire2, a3TireReussi2, a3TireRecu2);
    END IF;
    IF a4Tire2 > 0 OR a4TireReussi2 > 0 OR a4TireRecu2 > 0 THEN
        INSERT INTO partie_utilisateur_arme(id_partie_utilisateur, id_arme, nb_tire, nb_tire_reussi, nb_coup_recu)
            VALUES(id_partie_Joueur2, 4, a4Tire2, a4TireReussi2, a4TireRecu2);
    END IF;
    IF a5Tire2 > 0 OR a5TireReussi2 > 0 OR a5TireRecu2 > 0 THEN
        INSERT INTO partie_utilisateur_arme(id_partie_utilisateur, id_arme, nb_tire, nb_tire_reussi, nb_coup_recu)
            VALUES(id_partie_Joueur2, 5, a5Tire2, a5TireReussi2, a5TireRecu2);
    END IF;
    IF a6Tire2 > 0 OR a6TireReussi2 > 0 OR a6TireRecu2 > 0 THEN
        INSERT INTO partie_utilisateur_arme(id_partie_utilisateur, id_arme, nb_tire, nb_tire_reussi, nb_coup_recu)
            VALUES(id_partie_Joueur2, 6, a6Tire2, a6TireReussi2, a6TireRecu2);
    END IF;
END;
/

--EXECUTE insert_Partie(27, 28, 0, 0, 27, 1, sysdate, sysdate, 100, 100,   1, 1, 1,  1, 1, 1,  1, 1, 1,  1, 1, 1,  1, 1, 1,  1, 1, 1,   1, 1, 1,  1, 1, 1,  1, 1, 1,  1, 1, 1,  1, 1, 1,  1, 1, 1);

COMMIT;