 --Mes tables sont en 3 FN, car elles sont atomique, aucune valeur n'est composé de deux
--valeurs qui pourrait être séparé. Toute les colones dépendent entièrement de la clé primaire (ici le ID).
--Le niveau n'est pas dans la bd, car il peut etre calculer par l'exp, même chose pour le nombre de CoolPoints


DROP TABLE utilisateur;

CREATE TABLE utilisateur(
 id                         NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
 nom                        VARCHAR2(30) NOT NULL ,
 mdp                        VARCHAR2(30) NOT NULL ,
 exp                        NUMBER NOT NULL ,
 motto                      VARCHAR2(255) NOT NULL ,
 couleur                    RAW(6) NOT NULL ,
 vie                        NUMBER NOT NULL ,
 force                      NUMBER NOT NULL ,
 agilite                    NUMBER NOT NULL ,
 dexterite                  NUMBER NOT NULL ,

 CONSTRAINT PK_utilisateur PRIMARY KEY (id),
 CONSTRAINT constraint_nom_unique UNIQUE (nom)
);

CREATE OR REPLACE PROCEDURE insert_Utilisateur (nom VARCHAR2, mdp VARCHAR2, motto VARCHAR2, couleur RAW,
    id_Joueur OUT NUMBER) IS
BEGIN
    IF LENGTH(nom) <= 30 OR LENGTH(mdp) <= 30 THEN
        INSERT INTO utilisateur(nom, mdp, exp, motto, couleur, vie, force, agilite, dexterite) VALUES(nom, mdp, 0, motto, couleur, 0, 0, 0, 0)
        RETURNING id into id_Joueur;
    ELSE
        RAISE VALUE_ERROR;
    END IF;
    DBMS_OUTPUT.PUT_LINE(id_Joueur);
END;
/

--VARIABLE idJoueur NUMBER;
--EXECUTE insert_Utilisateur('allo', 'a', 'NON!', 'AAAAAA', :idJoueur);

CREATE OR REPLACE PROCEDURE update_Utilisateur (p_nom VARCHAR2, p_motto VARCHAR2, p_couleur RAW, p_vie NUMBER, p_force NUMBER, p_agilite NUMBER, p_dexterite NUMBER) IS
BEGIN
    IF p_vie <= 20 AND p_vie >= 0 AND p_force <= 20 AND p_force >= 0 AND p_agilite <= 20 AND p_agilite >= 0 AND p_dexterite <= 20 AND p_dexterite >= 0 THEN
        UPDATE utilisateur SET motto = p_motto, couleur = p_couleur, vie = p_vie, force = p_force, agilite = p_agilite, dexterite = p_dexterite WHERE nom = p_nom;
    ELSE
        RAISE VALUE_ERROR;
    END IF;
END;
/

--EXECUTE update_Utilisateur('ALLO', 'DFGDFGDFG', 'ABCDEF', 1, 1, 1, 1);

COMMIT;
