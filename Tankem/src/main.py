## -*- coding: utf-8 -*-

#Ajout des chemins vers les librarires
from util import inclureCheminCegep
import sys

#Importe la configuration de notre jeu
from panda3d.core import loadPrcFile
loadPrcFile("config/ConfigTankem.prc")

#Module de Panda3DappendObject
from direct.showbase.ShowBase import ShowBase

#Modules internes
from gameLogic import GameLogic
from interface import InterfaceMenuPrincipal

#Module de la balande
from balance import DAO
from balance import util
import datetime

import ctypes #messages derreure   

class Tankem(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        # insertion des variable de jeu
        # if(DAO.readValues):
        self.DAObalance = DAO.DAObalance()
        self.temp = self.DAObalance.readValues()

        #vérifier si on ce connecte a la bd
        if self.temp != False:
            self.variables = self.temp.getDict()         
        else:
            ctypes.windll.user32.MessageBoxA(0, "Il est impossible de ce connecter a la base de donne, on vas loader les valeurs par default", "Imposible de connecter a la bd", 0)
            self.sanitizer = util.Sanitizer()
            self.variables = self.sanitizer.getDefault()

        # print(self.variables)
        # print(self.variables['Vitesse des chars'][0])
        # print(self.variables['Message fin de partie'][0])
        print(str(datetime.datetime.now()))
        

        # démarer le jeu
        self.demarrer()

    def demarrer(self):
        self.gameLogic = GameLogic(self)
        #Commenter/décommenter la ligne de votre choix pour démarrer le jeu
        #Démarre dans le menu
        self.menuPrincipal = InterfaceMenuPrincipal()
        #Démarre directement dans le jeu
        # messenger.send("DemarrerPartie", [1])

#Main de l'application.. assez simple!
app = Tankem()
app.run()