#coding: utf-8
import cx_Oracle
import Constants as co

class Connect(object):
	class __Connect(object):
		def __init__(self, username, password, hostname, port, database):
			self.username = username
			self.password = password
			self.hostname = hostname
			self.port = port
			self.database = database

		""" CONNECTION BD """ 
		def connectionOracle(self):
			dsn_tns = cx_Oracle.makedsn(self.hostname, self.port, self.database )
			chaineConnexion = self.username + '/' + self.password + '@' + dsn_tns
			try:
				connexion = cx_Oracle.connect(chaineConnexion)
				return connexion
			except cx_Oracle.DatabaseError as exception:
				print(exception)
				print('Erreur de Connexion \n')
				return False

		""" DECONNEXION BD """
		def disconnect(self):
			try:
				self.database.close()
			except cx_Oracle.DatabaseError:
				print ("Erreur déconnexion")
				raiseaw_log('Critical', value)

	__instance = None

	def __new__(cls):
		if not Connect.__instance:
			Connect.__instance = Connect.__Connect(co.USER_DB, co.USER_PASS, co.HOSTNAME, co.PORT, co.DB)
		return Connect.__instance