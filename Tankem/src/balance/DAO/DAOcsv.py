#coding: utf-8
import time
import csv
import os.path
from DAObalance import *

class DAOcsv(object):
	def __init__(self):
		pass
	def EcrireCsv(self,file,file0):
		writer = Writer()
		dao = DAObalance()
		dto = dao.readValues() ## lire dans la base de donn�e

		dictionnaire = dto.getDict()
		tab = []
		i=0
		for cle in dictionnaire:
			row = dto.getValeur(cle)
			temp=[cle]
			for valeur in row:
				temp.append(valeur)
			tab.append(temp)

		if writer.writeCsv(tab,file0+file):
			return True
		else:
			return False
		

	def LectureCSV(self,file,filePath):
		reader = Reader()
		values = reader.readCsv(filePath+file)
		if values != False:
			#print(values) ### ecrire les values dans la base de donnes
			dto = DTObalance()
			for row in values:
				temp = {}
				if len(row) == 4:
					temp[row[0]] = (row[1], row[2], row[3])
				else:
					temp[row[0]] = (row[1], row[2])

				dto.setValeur(row[0],temp[row[0]])
			dao = DAObalance()
			dao.updateData(dto) #Insertion des donnees dans la BD 
		else :
			print(' Une erreur est survenue! avez vous utilise un CSV valide ? \n')
		
class Reader():
	def __init__(self):
		pass

	def readCsv(self,file):
		print(file)
		if CsvExistant(file) and estCsv(file):
			with open(file,'r') as csv_file:
				csv_reader = csv.reader(csv_file)
				values = []
				for line in csv_reader:
					values.append(line)
			return values
		else :
			print("Erreur avec le fichier CSV")
			return False

class Writer():
	def __init__(self):
		pass
	
	def writeCsv(self,values,file):
		if estCsv(file): 
			with open(file,'wb') as csv_file:  ## peut etre utiliser 'w' pour toujour tout supprimer dans le csv
				csv_writer = csv.writer(csv_file)
				for line in values:
					csv_writer.writerow(line)
			return True
		else:
			print("Erreur avec le fichier CSV")
			return False

def estCsv(csv):
	if csv.endswith('.csv'):
		return True
	else :
		return False

def CsvExistant(csv):
	#my_file = Path(csv)
	#if my_file.is_file():
	if os.path.exists(csv):
		return True
	else :
		return False