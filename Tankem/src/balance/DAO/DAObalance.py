#coding: utf-8
import cx_Oracle
from DTObalance import *
from singleton_connection import *

""" DAObalance réalise les intéractions avec la BD """
class DAObalance(object):

    #constructor    
    def __init__(self):
        self.conn = Connect();
        self.connexion = self.conn.connectionOracle()

    def update(self, dto):
        

        """ UPDATE """
        """ update la bd grace au DTO crée dans Ecriture.py """
    def updateData(self, dto):
        noms = ["Message signal debut de partie", "Message fin de partie", "Message d accueil"]
        listeParam = []
        listeMessage = []

        for key in dto.getDict().items():
            if key[0] == noms[0] or key[0] == noms[1] or key[0] == noms[2]:
                print("--message--\n")
                message = {}
                message[key[0]] = (key[1][0], key[1][1], key[0])
                listeMessage.append(message[key[0]])
                print(message[key[0]])
            else:
                print("--parametre--\n")
                parametre = {}
                parametre[key[0]] = (float(key[1][0]), float(key[1][1]), float(key[1][2]), key[0])
                listeParam.append(parametre[key[0]])
                print(parametre[key[0]])

            print("\n************************\n")
        
        try:
            if self.connexion != False :
                cur = self.connexion.cursor()
            else : 
                print("\nErreur de connexion - updateData\n")
                return False
            print("\n****UPDATE MESSAGES****\n")
            query = 'UPDATE message SET MESSAGE = :0, MAXIMUM_CHAR = :1 WHERE NOM = :2'
            cur.executemany(query, listeMessage)

            print("\n****UPDATE PARAMETRES****\n")
            query2 = 'UPDATE PARAMETRE SET MINIMUM = :0, MAXIMUM = :1, COURANTE = :2  WHERE NOM = :3'
            cur.executemany(query2, listeParam)
            cur.close()
            self.connexion.commit()
        except cx_Oracle.DatabaseError as exception:
            print(exception)
            print('\nErreur updateData\n')
        self.conn.disconnect()



    """ INSERT """
    """ Cette methode est appelée si au préalable l'update n'a pas fonctionné (par exemple si la ligne n'existe pas dans la table) """
    def insertData(liste):
        try:
            if self.connexion != False :
                cur = self.connexion.cursor()
            else :
                print("Erreur de connexion - insertData")
                return False
            query = "INSERT INTO dictionnaire VALUES (:1)"
            cur.executemany(query, liste, batcherrors=True)
            cur.close()
            self.connexion.commit()
        except cx_Oracle.DatabaseError as exception:
            print('Erreur insertData')
            print(exception)
            return False
        self.conn.disconnect()






    """ LIRE VALEURS """
    """ Retourne un tableau avec les valeurs des paramètres """    
    def readValues(self):
        tableauDonnees = {}
        dto = DTObalance()
        try:
            if self.connexion != False :
                cur = self.connexion.cursor()
            else : 
                return False
            """MESSAGES"""
            cur.execute('SELECT nom, message, maximum_char FROM message')
            messages = cur.fetchall()
            for i in messages:
                tableauDonnees[i[0]] = (i[1], i[2])
                dto.setValeur(i[0], tableauDonnees[i[0]]) 

            """PARAMETRES"""
            cur.execute('SELECT nom, courante, minimum, maximum FROM parametre')
            parametres = cur.fetchall()
            for i in parametres:
                tableauDonnees[i[0]]= (i[1], i[2], i[3]) 
                dto.setValeur(i[0], tableauDonnees[i[0]])
            cur.close()
            self.connexion.commit()
            return dto
        except cx_Oracle.DatabaseError as exception:
            print('Erreur readValues BD \n')
            print (exception)
            return False
        self.conn.disconnect()
        



        