#coding: utf-8

class Sanitizer:
	def __init__(self):
		self._default = {
			'Vitesse des chars': [7,4,12],
			'Vitesse de rotation des chars': [1500,1000,2000],
			'Points de vie des chars': [200,100,2000],
			'Temps du mouvement des blocs animes': [1,0,2],
			'Canon - Vitesse balle': [14,4,30],
			'Canon - Temps de recharge': [1,0,10],
			'Mitraillette - Vitesse balle': [18,4,30],
			'Mitraillette - Temps de recharge': [0,0,10],
			'Grenade - Vitesse initiale balle': [16,10,25],
			'Grenade - Temps de recharge': [1,0,10],
			'Shotgun - Vitesse balle': [13,4,30],
			'Shotgun - Temps de recharge': [2,0,10],
			'Shotgun - Ouverture du fusil': [0,0,2],
			'Piege - Vitesse balle': [1,0,4],
			'Piege - Temps de recharge': [1,0,10],
			'Missile guide Vitesse guidee balle': [30,20,40],
			'Missile guide - Temps de recharge': [3,0,10],
			'Spring - Vitesse initiale du saut': [10,6,20],
			'Spring - Temps de recharge': [1,0,10],
			'Grosseur de l explosion des balles': [8,1,30],
			'Message d accueil duree': [3,1,10],
			'Message compte a rebour duree': [3,0,10],



			'Message d accueil': ['Appuyer sur F1 pour l aide',60],
			'Message signal debut de partie': ['Tankem!',50],
			'Message fin de partie': ['Joueur :x a gagne!',70]

			}
				
	@property
	def default(self):
		return self._default

	@default.setter
	def default(self, val):
		self._default = val

	def getDefault(self):
		return self._default

	def displayDefault(self):
		# print(self._default)
		
		for x in self._default:
			print (str(x)+' : '+str(self._default[x]))

def main():
	test = Sanitizer()
	test.displayDefault()


if __name__ == "__main__":
    main()