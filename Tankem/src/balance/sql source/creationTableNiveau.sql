DROP TABLE parametre;
DROP TABLE message;

CREATE TABLE parametre(
 id       NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
 nom      VARCHAR(255) NOT NULL ,
 minimum      NUMBER NOT NULL ,
 maximum      NUMBER NOT NULL ,
 courante NUMBER NOT NULL ,

 CONSTRAINT PK_parametre PRIMARY KEY (id)
);

CREATE TABLE message(
 id       NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
 nom      VARCHAR(255) NOT NULL ,
 message  VARCHAR(4000) NOT NULL ,
 maximum_char NUMBER NOT NULL ,

 CONSTRAINT PK_message PRIMARY KEY (id)
);

CREATE OR REPLACE TRIGGER before_update_parametre 
BEFORE UPDATE ON parametre FOR EACH ROW
DECLARE
    v_min NUMBER;
BEGIN
	IF (:NEW.maximum IS NOT NULL and :NEW.minimum IS NOT NULL) THEN
		IF (:NEW.maximum < :NEW.minimum) THEN
			v_min := :NEW.minimum;
			:NEW.minimum := :NEW.maximum;
			:NEW.maximum := v_min;
		END IF;
		IF (:NEW.courante < :NEW.minimum) THEN
			:NEW.courante := :NEW.minimum;
		END IF;
		IF (:NEW.courante IS NOT NULL) THEN
			IF (:NEW.courante < :NEW.minimum) THEN
				:NEW.courante := :NEW.minimum;
			END IF;
			IF (:NEW.courante > :NEW.maximum) THEN
				:NEW.courante := :NEW.maximum;
        END IF;
    END IF;
	ELSE
		IF (:NEW.courante IS NOT NULL) THEN
			IF (:NEW.courante < :OLD.minimum) THEN
				:NEW.courante := :OLD.minimum;
			END IF;
			IF (:NEW.courante > :OLD.maximum) THEN
				:NEW.courante := :OLD.maximum;
			END IF;
		END IF;
	END IF;
    
END;
/

CREATE OR REPLACE TRIGGER before_insert_parametre 
BEFORE INSERT ON parametre FOR EACH ROW
DECLARE
    v_min NUMBER;
BEGIN
    IF (:NEW.maximum < :NEW.minimum) THEN
        v_min := :NEW.minimum;
        :NEW.minimum := :NEW.maximum;
        :NEW.maximum := v_min;
    END IF;
    IF (:NEW.courante < :NEW.minimum) THEN
        :NEW.courante := :NEW.minimum;
    END IF;
    IF (:NEW.courante > :NEW.maximum) THEN
        :NEW.courante := :NEW.maximum;
    END IF;
END;
/

CREATE OR REPLACE TRIGGER before_update_message 
BEFORE UPDATE ON message FOR EACH ROW
DECLARE
    v_max NUMBER;
BEGIN
    IF (:NEW.maximum_char IS NOT NULL) THEN
        IF (:NEW.maximum_char > 4000) THEN
            v_max  := 4000;
            :NEW.maximum_char := 4000;
        ELSE
            v_max  := :NEW.maximum_char;
        END IF;
    ELSE
        v_max  := :OLD.maximum_char;
    END IF;
    IF (LENGTH(:NEW.message) > v_max) THEN
        :NEW.message := SUBSTR( :NEW.message, 0 , v_max);
    END IF;
END;
/

INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Vitesse des chars', 4.0, 12.0, 7.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Vitesse de rotation des chars', 1000.0, 2000.0, 1500.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Points de vie des chars', 100.0, 2000.0, 200.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Temps du mouvement des blocs animes', 0.2, 2.0, 0.8);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Canon - Vitesse balle', 4.0, 30.0, 14.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Canon - Temps de recharge', 0.2, 10.0, 1.2);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Mitraillette - Vitesse balle', 4.0, 30.0, 18.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Mitraillette - Temps de recharge', 0.2, 10.0, 0.4);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Grenade - Vitesse initiale balle', 10.0, 25.0, 16.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Grenade - Temps de recharge', 0.2, 10.0, 0.8);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Shotgun - Vitesse balle', 4.0, 30.0, 13.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Shotgun - Temps de recharge', 0.2, 10.0, 1.8);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Shotgun - Ouverture du fusil', 0.1, 1.5, 0.4);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Piege - Vitesse balle', 0.2, 4.0, 1.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Piege - Temps de recharge', 0.2, 10.0, 0.8);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Missile guide Vitesse guidee balle', 20.0, 40.0, 30.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Missile guide - Temps de recharge', 0.2, 10.0, 3.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Spring - Vitesse initiale du saut', 6.0, 20.0, 10.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Spring - Temps de recharge', 0.2, 10.0, 0.5);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Grosseur de l explosion des balles', 1.0, 30.0, 8.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Message d accueil duree', 1.0, 10.0, 3.0);
INSERT INTO parametre (nom, minimum, maximum, courante)
VALUES ('Message compte a rebour duree', 0.0, 10.0, 3.0);

INSERT INTO message (nom, message, maximum_char) VALUES ('Message d accueil', 'Appuyer sur F1 pour l aide', 60);
INSERT INTO message (nom, message, maximum_char)
VALUES ('Message signal debut de partie', 'Tankem!', 50);
INSERT INTO message (nom, message, maximum_char)
VALUES ('Message fin de partie', 'Joueur :x a  gagne!' ,70);
COMMIT;